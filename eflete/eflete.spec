Name:           eflete
Version:        1.19.1
Release:        1%{?dist}
Summary:        Enlightenment Theme Tool

License:        LGPL3
URL:            http://enlightenment.org
Source0:        https://download.enlightenment.org/rel/apps/eflete/%{name}-%{version}.tar.xz

BuildRequires:  efl-devel

%description
A tool to make enlighenment themes


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Fri Sep 28 2018 L.L.Robinson <baggypants@fedoraproject.org>
- 
