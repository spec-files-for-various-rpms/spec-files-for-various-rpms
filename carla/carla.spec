%global debug_package %{nil}
Name:           Carla
Version:        1.9.6
Release:        3%{?dist}
Summary:        Carla Plugin Host

License:        GPLv2
#also LGPL
URL:            http://kxstudio.linuxaudio.org/Applications:Carla
Source0:       	https://github.com/falkTX/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz 
Patch0:		Carla-libdir.patch

BuildRequires:  python3-PyQt4-devel file-libs liblo-devel alsa-lib-devel pulseaudio-libs-devel
BuildRequires:	libX11-devel gtk2-devel gtk3-devel 
Requires:       python3-PyQt4

%description
Carla is a plugin host for many different plugins such as LV2, DSSI and others

%prep
%setup -q

%patch0 -p1

%build
#configure
make features
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%make_install PREFIX=/usr LIBDIR=/%{_lib}


%files
%doc

%{_bindir}/carla
%{_bindir}/carla-database
%{_bindir}/carla-patchbay
%{_bindir}/carla-rack
%{_bindir}/carla-settings
%{_bindir}/carla-single
%{_includedir}/carla/CarlaBackend.h
%{_includedir}/carla/CarlaEngine.hpp
%{_includedir}/carla/CarlaHost.h
%{_includedir}/carla/CarlaNative.h
%{_includedir}/carla/CarlaPlugin.hpp
%{_includedir}/carla/includes/CarlaDefines.h
%{_datarootdir}/applications/carla.desktop
%{_datarootdir}/carla
%{_datarootdir}/icons/hicolor/*/apps/carla*
%{_libdir}/carla
%{_libdir}/lv2/carla.lv2
%{_libdir}/vst/carla.vst
%{_libdir}/pkgconfig/carla-standalone.pc
%{_datarootdir}/mime/packages/carla.xml



%changelog
* Tue Nov 17 2015 L.L.Robinson <baggypants@fedoraproject.org> - 1.9.6-3
- new patch for incorrect directories

* Tue Nov 17 2015 L.L.Robinson <baggypants@fedoraproject.org> - 1.9.6-2
- added missing Requires

* Tue Nov 17 2015 L.L.Robinson <baggypants@fedoraproject.org>
- 
