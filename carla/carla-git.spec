%global debug_package %{nil}
%global commit0 5a95517b3e72a9d6eee48ae7bbe9d49198bc58c3
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
Name:           Carla
Version:        2.0beta5
Release:        9.%{shortcommit0}%{?dist}
Summary:        Plugin Host and JACK connection kit

License:        GPLv2
#also LGPL
URL:            http://kxstudio.linuxaudio.org/Applications:Carla
Source0:        https://github.com/falkTX/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires:  python3-PyQt4-devel file-libs liblo-devel alsa-lib-devel pulseaudio-libs-devel
BuildRequires:	libX11-devel gtk2-devel gtk3-devel zlib-devel fluidsynth-devel fftw-devel
BuildRequires:	mxml-devel non-ntk-devel libprojectM-devel non-ntk-fluid
Requires:       python3-PyQt4 lv2%{?_isa}

%description
Carla is a plugin host for many different plugins such as LV2, DSSI and others
as well as a JACK connection kit.

%package devel
Summary:	Files associated with carla for development
Group:		Developments/Libraries
Requires:	pkgconfig
Requires:	%{name} = %{version}-%{release}

%description devel
Packages for the carla plugin host for development use.

%prep
%autosetup -n %{name}-%{commit0}


%build
make features
make DESTDIR=%{buildroot} PREFIX=/usr LIBDIR=%{_libdir} %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%make_install PREFIX=/usr LIBDIR="%{_libdir}"

%post 
update-desktop-database -q
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
update-desktop-database -q
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans 
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%doc
%{_bindir}/carla
%{_bindir}/carla-database
%{_bindir}/carla-patchbay
%{_bindir}/carla-rack
%{_bindir}/carla-settings
%{_bindir}/carla-single
%{_bindir}/carla-control
%{_datarootdir}/applications/carla.desktop
%{_datarootdir}/carla
%{_datarootdir}/icons/hicolor/*/apps/carla*
%{_datarootdir}/applications/carla-control.desktop
%{_libdir}/carla
%{_libdir}/lv2/carla.lv2
%{_libdir}/vst/carla.vst
%{_datarootdir}/mime/packages/carla.xml
%{_libdir}/python3/dist-packages/carla*.py*

%files	devel
%{_libdir}/pkgconfig/carla-standalone.pc
%{_libdir}/pkgconfig/carla-utils.pc
%{_includedir}/carla/*

%changelog
* Fri Dec 23 2016 L.L.Robinson <baggypants@fedoraproject.org> - 1.9.6-9.58d3861
- updated Buildrequires and commit ver

* Fri Jan 01 2016 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 1.9.6-8.20160101git70c4ad10
- Update to git: 70c4ad10

* Sat Dec 05 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 1.9.6-7.20151205git6edc2a6f
- Update to git: 6edc2a6f

* Thu Nov 19 2015 L.L.Robinson <baggypants@fedoraproject.org> - 1.9.6-6.20151118git12db7be2
- split out -devel ad zlib

* Thu Nov 19 2015 L.L.Robinson <baggypants@fedoraproject.org>
- split out -devel ad zlib

* Wed Nov 18 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 1.9.6-4.20151118git12db7be2
- Update to git: 12db7be2

* Tue Nov 17 2015 L.L.Robinson <baggypants@fedoraproject.org> - 1.9.6-3
- new patch for incorrect directories

* Tue Nov 17 2015 L.L.Robinson <baggypants@fedoraproject.org> - 1.9.6-2
- added missing Requires

* Tue Nov 17 2015 L.L.Robinson <baggypants@fedoraproject.org>
-
