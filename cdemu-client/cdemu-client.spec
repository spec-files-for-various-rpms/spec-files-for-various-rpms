Summary: CDEmu CLI client
Name: cdemu-client
Version: 3.1.0
Release: 1%{?dist}
License: GPLv2
Group: Applications/System
URL: http://cdemu.sourceforge.net/about/client/
Source: http://downloads.sourceforge.net/cdemu/%{name}-%{version}.tar.bz2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

BuildRequires: intltool
BuildRequires: cmake

Requires: python
Requires: pygobject3

%description
cdemu-client is a command-line interface client for controlling CDEmu devices.
It is papt of CDEmu, a CD/DVD-ROM device emulator for linux.

%prep
%setup -q

%build
%cmake -DPOST_INSTALL_HOOKS=off .
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang cdemu

%clean
rm -rf $RPM_BUILD_ROOT

%post
/usr/bin/update-desktop-database &> /dev/null || :

%postun
/usr/bin/update-desktop-database &> /dev/null || :

%files -f cdemu.lang
%defattr(-, root, root, -)
%doc AUTHORS COPYING README ChangeLog
%doc %_mandir/man1/*
%{_bindir}/*
%{_datadir}/applications/*
%{_datadir}/pixmaps/*
%{_sysconfdir}/bash_completion.d/*

%changelog
* Mon Sep 10 2018 L.L.Robinson <baggypants@fedoraproject.org> - 3.1.0-1
- bump for new ver

* Wed Jun 22 2016 L.L.Robinson <baggypants@fedoraproject.org> - 3.0.1-1
- new version

* Wed Nov 04 2015 L.L.Robinson <baggypants@fedoraproject.org> - 3.0.0-2
- added cmake build dep

* Sun Jun 29 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.0-1
- Updated to 3.0.0

* Thu Sep 19 2013 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.1.1-1
- Updated to 2.1.1

* Fri Jun  7 2013 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.1.0-1
- Updated to 2.1.0

* Mon Dec 24 2012 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.0.0-1
- RPM release for new version
