Name:		setbfree
Version:	git
Release:        24.20160101gitc5255d09%{?dist}
Summary:	A DSP Tonewheel Organ emulator

Group:		Applications/Music
License:	GPL2
URL:		http://setbfree.org/
Source0:        setbfree-c5255d09.tar

BuildRequires:jack-audio-connection-kit-devel lv2-devel ftgl-devel mesa-libGLU-devel bitstream-vera-fonts-common bitstream-vera-sans-fonts help2man alsa-lib-devel zita-convolver-devel libsndfile-devel fftw-devel desktop-file-utils
Requires:	jack-audio-connection-kit lv2 mesa-libGLU bitstream-vera-fonts-common alsa-lib bitstream-vera-sans-fonts zita-convolver libsndfile fftw-devel

%description
A DSP Tonewheel Organ emulator, based on code from beatrix

%prep
%setup -q -n setbfree


%build

make FONTFILE=/usr/share/fonts/bitstream-vera/VeraBd.ttf ENABLE_CONVOLUTION=yes
#make doc


%install
make install DESTDIR=%{buildroot} bindir=%{_bindir} lv2dir=%{_libdir}/lv2 sharedir=%{_datadir}/setBfree FONTFILE=/usr/share/fonts/bitstream-vera/VeraBd.ttf ENABLE_CONVOLUTION=yes
mkdir -p %{buildroot}/%{_mandir}/man1
cp doc/*.1 %{buildroot}/%{_mandir}/man1/


%files
%doc README.md COPYING
%{_mandir}/man1/jboverdrive.1*
%{_mandir}/man1/setBfree.1*
%{_mandir}/man1/setBfreeUI.1*
%{_mandir}/man1/x42-whirl.1*
#%{_bindir}/jboverdrive
%{_bindir}/setBfree
%{_bindir}/setBfreeUI
%_libdir/lv2/b_overdrive/b_overdrive.so
%_libdir/lv2/b_overdrive/b_overdrive.ttl
%_libdir/lv2/b_overdrive/manifest.ttl
%_libdir/lv2/b_reverb/b_reverb.so
%_libdir/lv2/b_reverb/b_reverb.ttl
%_libdir/lv2/b_reverb/manifest.ttl
%_libdir/lv2/b_synth/b_synth.so
%_libdir/lv2/b_synth/b_synth.ttl
%_libdir/lv2/b_synth/b_synthUI.so
%_libdir/lv2/b_synth/manifest.ttl
%_libdir/lv2/b_whirl/b_whirl-configurable.ttl
#%_libdir/lv2/b_whirl/b_whirl-presets.ttl
%_libdir/lv2/b_whirl/b_whirl.so
%_libdir/lv2/b_whirl/b_whirl.ttl
%_libdir/lv2/b_whirl/manifest.ttl
%{_datadir}/setBfree/cfg/K2500.cfg
%{_datadir}/setBfree/cfg/bcf2000.cfg
%{_datadir}/setBfree/cfg/cx3.cfg
%{_datadir}/setBfree/cfg/default.cfg
%{_datadir}/setBfree/cfg/fartybass.cfg
%{_datadir}/setBfree/cfg/osc-bassy.cfg
%{_datadir}/setBfree/cfg/osc-middy.cfg
%{_datadir}/setBfree/cfg/osc-noxtalk.cfg
%{_datadir}/setBfree/cfg/osc-sawtooth.cfg
%{_datadir}/setBfree/cfg/osc-scoop.cfg
%{_datadir}/setBfree/cfg/osc-square.cfg
%{_datadir}/setBfree/cfg/osc-trebly.cfg
%{_datadir}/setBfree/cfg/osc-triangle.cfg
%{_datadir}/setBfree/cfg/osc-xwave.cfg
%{_datadir}/setBfree/cfg/oxy61.cfg
%{_datadir}/setBfree/cfg/whirl-mellow.cfg
%{_datadir}/setBfree/cfg/whirl-sharp.cfg
%{_datadir}/setBfree/cfg/whirl-x100.cfg
%{_datadir}/setBfree/ir/ir_leslie-44100.wav
%{_datadir}/setBfree/ir/ir_leslie-48000.wav
%{_datadir}/setBfree/pgm/default.pgm
%{_datadir}/setBfree/pgm/popular.pgm





%changelog
* Fri Jan 01 2016 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-24.20160101gitc5255d09
- Update to git: c5255d09

* Sat Dec 05 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-23.20151205git778316b8
- Update to git: 778316b8

* Wed Nov 18 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-22.20151118gitab2baa42
- Update to git: ab2baa42

* Sat Aug 22 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-21.20150822git5202a2e3
- Update to git: 5202a2e3

* Tue Aug 11 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-20.20150811git1f9a0a44
- Update to git: 1f9a0a44

* Sun Aug 02 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-19.20150802git437629e7
- Update to git: 437629e7

* Sat Jun 27 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-18.20150627gitfc780e7d
- Update to git: fc780e7d

* Tue Jun 16 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-17.20150609git91845daf
- attempt to put things in the correct directories

* Tue Jun 09 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-16.20150609git91845daf
- Update to git: 91845daf

* Sat May 30 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-15.20150530gitfb0e6260
- Update to git: fb0e6260

* Sun Apr 19 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-13.20150419git0bfbdb66
- Update to git: 0bfbdb66

* Thu Apr 09 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-12.20150409git6dc0d675
- Update to git: 6dc0d675

* Wed Mar 11 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-11.20150311git64133140
- Update to git: 64133140

* Wed Mar 04 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-10.20150304gitaf84ab3c
- Update to git: af84ab3c

* Wed Feb 25 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-9.20150225gitfb2d5fbd
- idiotic typo

* Wed Feb 25 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-8.20150225gitfb2d5fbd
- enable convolution

* Wed Feb 25 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-7.20150225gitfb2d5fbd
- Update to git: fb2d5fbd

* Sat Feb 21 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-5.20150221git80b16c43
- Update to git: 80b16c43

* Mon Feb 16 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-4.20150216git0dff16ff
- fixed a very silly typo in Requires

* Mon Feb 16 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-2.20150216git0dff16ff
- Update to git: 0dff16ff


