Name:		setBfree
Version:	0.8.4
Release:        2%{?dist}
Summary:	A DSP Tonewheel Organ emulator

Group:		Applications/Music
License:	GPL2
URL:		http://setbfree.org/
Source0:        https://github.com/pantherb/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:	 %{name}.desktop

BuildRequires:jack-audio-connection-kit-devel lv2-devel ftgl-devel mesa-libGLU-devel bitstream-vera-fonts-common bitstream-vera-sans-fonts help2man alsa-lib-devel zita-convolver-devel libsndfile-devel fftw-devel desktop-file-utils
Requires:	jack-audio-connection-kit lv2 mesa-libGLU bitstream-vera-fonts-common alsa-lib bitstream-vera-sans-fonts zita-convolver libsndfile fftw-devel

%description
A DSP Tonewheel Organ emulator, based on code from beatrix

%prep
%setup -q


%build

make FONTFILE=/usr/share/fonts/bitstream-vera/VeraBd.ttf ENABLE_CONVOLUTION=yes
#make doc


%install
make install DESTDIR=%{buildroot} bindir=%{_bindir} lv2dir=%{_libdir}/lv2 sharedir=%{_datadir}/setBfree FONTFILE=/usr/share/fonts/bitstream-vera/VeraBd.ttf ENABLE_CONVOLUTION=yes
mkdir -p %{buildroot}/%{_mandir}/man1
cp doc/*.1 %{buildroot}/%{_mandir}/man1/
mkdir -p %{buildroot}%{_datadir}/pixmaps
cp doc/setBfree.png %{buildroot}%{_datadir}/pixmaps/

desktop-file-install --dir=${RPM_BUILD_ROOT}%{_datadir}/applications %{SOURCE1}


%files
%doc README.md COPYING
%{_mandir}/man1/jboverdrive.1*
%{_mandir}/man1/setBfree.1*
%{_mandir}/man1/setBfreeUI.1*
%{_mandir}/man1/x42-whirl.1*
%{_bindir}/setBfree
%{_bindir}/setBfreeUI
%_libdir/lv2/b_overdrive/*
%_libdir/lv2/b_reverb/*
%_libdir/lv2/b_synth/*
%_libdir/lv2/b_whirl/*
%{_datadir}/setBfree/*
%{_datadir}/applications/setBfree.desktop
%{_datadir}/pixmaps/setBfree.png





%changelog
* Tue Mar 28 2017 L.L.Robinson <baggypants@fedoraproject.org> - 0.8.4-2
- updated to 0.8.4

* Wed Jan 27 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.8.1-1
- updated to latest version

* Mon Jun 29 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 1
- Stable Release 0.8.0
