Name:           pgallery
Version:        0.1
Release:        2%{?dist}
Summary:        A simpole photo gallery

License:        GPLv3
URL:            https://github.com/joancipria/%{name}
Source0:        %{url}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  meson vala gcc gtk3-devel desktop-file-utils
#Requires:       

%description
A dead simple photo gallery made for Pinephone

%prep
%autosetup


%build
%meson
%meson_build


%install
%meson_install
desktop-file-install --remove-category=Multimedia --add-mime-type=image/gif --add-mime-type=image/jpeg --add-mime-type=image/jpg --add-mime-type=image/png --delete-original --dir=%{buildroot}%{_datadir}/applications  %{buildroot}/%{_datadir}/applications/com.github.joancipria.pgallery.desktop
rm -f %{buildroot}/usr/lib/debug/usr/bin/com.github.joancipria.pgallery-0.1-1.fc33.x86_64.debug

%files
%license LICENSE
%doc README.md
%{_bindir}/com.github.joancipria.pgallery
%{_datadir}/applications/com.github.joancipria.pgallery.desktop
%{_datadir}/glib-2.0/schemas/com.github.joancipria.pgallery.gschema.xml




%changelog
* Sun Apr 18 2021 Baggypants <junk@therobinsonfamily.net> - 0.1-2
- added mimetype for registration

* Sun Apr 18 2021 Baggypants <junk@therobinsonfamily.net>
- 
