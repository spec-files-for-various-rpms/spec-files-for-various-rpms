Name:           calindori
Version:        21.08
Release:        1%{?dist}
Summary:        Calendar application for Plasma Mobile

License:        GPLv3+
URL:            https://invent.kde.org/plasma-mobile/%{name}
Source0:        %{url}/-/archive/v%{version}/%{name}-v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  cmake 
BuildRequires:  gcc-c++
BuildRequires:  extra-cmake-modules
BuildRequires:  hicolor-icon-theme
BuildRequires:  qconf
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qt3d-devel
BuildRequires:  qt5-qtquickcontrols2-devel
BuildRequires:  qt5-qtsvg-devel
BuildRequires:  desktop-file-utils
BuildRequires:  kf5-kirigami2-devel
BuildRequires:  kf5-kcalendarcore-devel
BuildRequires:  kf5-kconfig-devel
BuildRequires:  kf5-ki18n-devel
BuildRequires:  kf5-kcoreaddons
BuildRequires:  kf5-kdbusaddons-devel
BuildRequires:  kf5-knotifications-devel
BuildRequires:  kf5-kservice-devel
BuildRequires:  kf5-kpeople-devel
BuildRequires:  libappstream-glib

Requires:       qt5-qtwayland 
Requires:       kf5-kirigami2

%description
Plasma Calendar app

%prep
%autosetup -n %{name}-v%{version}


%build
%cmake .
%cmake_build


%install
%cmake_install
desktop-file-validate %{buildroot}/%{_sysconfdir}/xdg/autostart/org.kde.calindac.desktop
desktop-file-install --set-key=Exec --set-value="env QT_QUICK_CONTROLS_MOBILE=true QT_QUICK_CONTROLS_STYLE=Plasma calindori %f" --delete-original --dir=%{buildroot}/%{_datadir}/applications/  %{buildroot}/%{_datadir}/applications/org.kde.calindori.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/org.kde.calindori.appdata.xml

%files
%license COPYING
%doc README.md
%{_sysconfdir}/xdg/autostart/org.kde.calindac.desktop
%{_bindir}/calindac
%{_bindir}/calindori
%{_datadir}/applications/org.kde.calindori.desktop
%{_datadir}/dbus-1/services/org.kde.calindac.service
%{_datadir}/icons/hicolor/scalable/apps/calindori.svg
%{_datadir}/knotifications5/calindac.notifyrc
%{_metainfodir}/org.kde.calindori.appdata.xml



%changelog
* Tue Oct 12 2021 Baggypants <junk@therobinsonfamily.net> - 21.08-1
- new version

* Tue Apr 20 2021 Baggypants <junk@therobinsonfamily.net> - 1.4-3
- defudged the desktop file

* Tue Apr 20 2021 Baggypants <junk@therobinsonfamily.net> - 1.4-2
- new requires and edit launch

* Tue Apr 20 2021 Baggypants <junk@therobinsonfamily.net>
- 
