Name:           flam3
Version:        3.1.1
Release:        1%{?dist}
Summary:        cosmic recursive fractal flames

License:        GPL3
URL:            https://flam3.com
Source0:        https://github.com/scottdraves/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  gcc make zlib-devel libpng-devel libxml2-devel libjpeg-turbo-devel
#        

%description
Render fractal flames as described on http://flam3.com. 

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license COPYING
%doc README.txt
%{_bindir}/flam3-animate
%{_bindir}/flam3-convert
%{_bindir}/flam3-genome
%{_bindir}/flam3-render
%{_mandir}/man1/flam3-animate.1.gz
%{_mandir}/man1/flam3-convert.1.gz
%{_mandir}/man1/flam3-genome.1.gz
%{_mandir}/man1/flam3-render.1.gz
%{_datadir}/flam3/flam3-palettes.xml
%{_libdir}/libflam3.a
%{_libdir}/libflam3.la


%files devel
%{_includedir}/flam3.h
%{_includedir}/isaac.h
%{_includedir}/isaacs.h
%{_includedir}/rect.c
/usr/lib/debug/usr/bin/flam3-animate-3.1.1-1.fc30.x86_64.debug
/usr/lib/debug/usr/bin/flam3-convert-3.1.1-1.fc30.x86_64.debug
/usr/lib/debug/usr/bin/flam3-genome-3.1.1-1.fc30.x86_64.debug
/usr/lib/debug/usr/bin/flam3-render-3.1.1-1.fc30.x86_64.debug
%{_libdir}/pkgconfig/flam3.pc



%changelog
* Wed May  8 2019 L.L.Robinson <baggypants@fedoraproject.org>
- 
