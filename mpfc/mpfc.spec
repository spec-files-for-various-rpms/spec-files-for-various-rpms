%global commit0 2f0999c54158b1ddc9de8f75b27964c955ed8706
Name:           mpfc
Version:        2.0
Release:        1%{?dist}
Summary:        Music Player for Console

License:        GPL2+
URL:            https://code.google.com/archive/p/mpfc/
Source0:  	https://gitlab.com/Baggypants/%{name}/repository/archive.tar.gz?ref=v%{version}#/%{name}-%{version}.tar.gz


BuildRequires:  gstreamer1-devel, ncurses-devel, automake, libtool, taglib-devel
BuildRequires:	gettext-devel, gpm-devel, json-glib-devel, gstreamer1-plugins-base-devel
BuildRequires:	texinfo, byacc, flex
Requires:       info

%description
A console based music player which uses gstreamer and has a colourful interface.

%package devel
Summary:	mpfc header files
%description devel
Header files for developing applications which what to interact with mpfc


%prep
%setup -q -n %{name}-v%{version}-%{commit0}


%build
./autogen.sh
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%make_install
rm -f $RPM_BUILD_ROOT%{_datarootdir}/info/dir


%files
%doc README INSTALL AUTHORS
%license COPYING
%{_bindir}/mpfc
%{_libdir}/mpfc/*
%{_libdir}/libmpfc.*
%{_libdir}/libmpfcwnd.*
%{_mandir}/man1/mpfc.1.*
%{_datarootdir}/info/mpfc.info.*
%{_datarootdir}/locale/ru/LC_MESSAGES/mpfc.mo

%files devel
%{_includedir}/mpfc/*
%{_libdir}/pkgconfig/mpfc.pc

%changelog
* Wed Jun 29 2016 L.L.Robinson <baggypants@fedoraproject.org> - 2.0-1
- rebase source to gitlab and update version

* Wed Jun 29 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.01-2
- fix info dependency

* Tue Jun 28 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
