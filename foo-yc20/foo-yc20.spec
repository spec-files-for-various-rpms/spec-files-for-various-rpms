%global debug_package %{nil}
Name:           foo-yc20
Version:        1.3.0
Release:        2%{?dist}
Summary:        A Faust emulation on a Yamaha YC20 Combo organ

License:        MIT
URL:            https://github.com/sampov2/foo-yc20
Source0:        https://github.com/sampov2/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  cairo-devel jack-audio-connection-kit-devel gtk2-devel lv2-devel gcc-c++
Requires:       lv2%{?_isa}

%description


%prep
%autosetup


%build
#configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
#make_install
install -Dm 755 foo-yc20 $RPM_BUILD_ROOT/%{_bindir}/foo-yc20
install -Dm 755 foo-yc20-cli $RPM_BUILD_ROOT/%{_bindir}/foo-yc20-cli
install -d $RPM_BUILD_ROOT/%{_datadir}/foo-yc20/graphics
install -m 644 graphics/*.png $RPM_BUILD_ROOT/%{_datadir}/foo-yc20/graphics
cat foo-yc20.desktop.in | sed 's:%PREFIX%:{_datadir}:' > foo-yc20.desktop
install -Dm 644 foo-yc20.desktop $RPM_BUILD_ROOT/%{_datadir}/applications/foo-yc20.desktop
rm foo-yc20.desktop
install -d $RPM_BUILD_ROOT/%{_libdir}/lv2/foo-yc20.lv2
install -m 755 src/foo-yc20.lv2/*.so $RPM_BUILD_ROOT/%{_libdir}/lv2/foo-yc20.lv2
install -m 644 src/foo-yc20.lv2/*.ttl $RPM_BUILD_ROOT/%{_libdir}/lv2/foo-yc20.lv2


%files
%license LICENSE
%doc README
%{_bindir}/foo-yc20
%{_bindir}/foo-yc20-cli
%{_libdir}/lv2/foo-yc20.lv2
%{_datadir}/applications/foo-yc20.desktop
%{_datadir}/foo-yc20



%changelog
* Wed Oct 31 2018 L.L.Robinson <baggypants@fedoraproject.org> - 1.3.0-2
- updated buildrequires for copr/koji

* Mon Oct 15 2018 L.L.Robinson <baggypants@fedoraproject.org>
- 
