Summary: A CD-ROM image access library
Name: libmirage
Version: 3.0.3
Release: 1%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://cdemu.sourceforge.net/pkg_libmirage.php
Source: http://downloads.sourceforge.net/cdemu/%{name}-%{version}.tar.bz2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libsndfile-devel
BuildRequires: libsamplerate-devel
BuildRequires: glib2-devel
BuildRequires: gtk-doc
BuildRequires: zlib-devel
BuildRequires: bzip2-devel
BuildRequires: xz-devel
BuildRequires: gobject-introspection-devel
BuildRequires: cmake

%description
This is libMirage library, a CD-ROM image access library, and part of the
userspace-cdemu suite, a free, GPL CD/DVD-ROM device emulator for linux. The
library aims to provide uniform access to data stored in different image
formats by creating a representation of disc stored in the image file.

%package devel
Summary: A CD-ROM image access library
Group: Development/Libraries
Requires: pkgconfig
Requires: %{name} = %{version}-%{release}
Requires: gtk-doc

%description devel
This is libMirage library, a CD-ROM image access library, and part of the
userspace-cdemu suite, a free, GPL CD/DVD-ROM device emulator for linux. The
library aims to provide uniform access to data stored in different image
formats by creating a representation of disc stored in the image file.

This package contains files needed to develop with libMirage.

%prep
%setup -q

%build
%cmake -DPOST_INSTALL_HOOKS=off .
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
/sbin/ldconfig
update-mime-database %{_datadir}/mime &> /dev/null || :

%files
%defattr(-, root, root, -)
%doc AUTHORS COPYING README ChangeLog
%{_libdir}/libmirage.so.*
%{_libdir}/libmirage-*
%{_datadir}/mime/packages/*
%{_libdir}/girepository-1.0/*
%{_datadir}/gir-1.0/*

%files devel
%defattr(-, root, root, -)
%{_libdir}/lib*.so
%{_includedir}/*
%{_libdir}/pkgconfig/*
%doc %{_datadir}/gtk-doc/html/*

%changelog
* Sun Nov  9 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.3-1
- Updated to 3.0.2

* Sun Sep 28 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.2-1
- Updated to 3.0.2

* Fri Jul 25 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.1-1
- Updated to 3.0.1

* Sun Jun 29 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.0-1
- Updated to 3.0.0

* Thu Sep 19 2013 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.1.1-1
- Updated to 2.1.1

* Fri Jun  7 2013 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.1.0-1
- Updated to 2.1.0

* Mon Dec 24 2012 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.0.0-1
- RPM release for new version
