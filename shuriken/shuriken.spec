Name:		shuriken
Version:	0.5.2
Release:	2%{?dist}
Summary:	beat slicer

Group:		Testing
License:	GPLv2
URL:		https://github.com/rock-hopper/%{name}/
Source0:	https://github.com/rock-hopper/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:	jack-audio-connection-kit-devel, alsa-lib-devel, aubio-devel, liblo-devel, qt-devel, automake, libtool
BuildRequires:	rubberband-devel, libsamplerate-devel, libsndfile-devel, libuuid-devel 
# qt5-qtbase-devel
#Patch to enable compiling on gcc 6.2 
#Patch:		shuriken-gcc62.patch


%description


%prep
%autosetup 


%build

./build  --qt4

%install
mkdir -p "%{buildroot}/%{_bindir}/"
install -m 755 -p "release/shuriken" "%{buildroot}/%{_bindir}/shuriken"
#exit 1
%files
%doc
%{_bindir}/shuriken


%changelog
* Mon May 06 2019 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.2-2
- qlib changes

* Fri Oct 12 2018 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.2-1
- new version

* Thu Dec 08 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.1-2
- added compilation fix patch

* Tue Sep 22 2015 L.L.Robinson <baggypants@fedoraproject.org> - 0.5-1
- initial-version

