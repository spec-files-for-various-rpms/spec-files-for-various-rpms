Name:           dfc
Version:        3.0.5
Release:        10%{?dist}
Summary:        Display file system space usage using graph and colour
License:        BSD
URL:            http://projects.gw-computing.net/projects/%{name}
Source0:        http://projects.gw-computing.net/attachments/download/467/%{name}-%{version}.tar.gz

BuildRequires:  cmake gcc-c++


%description
dfc displays file system space usage using graphs and colors.
In some ways, it is a modernized version of df as it is able to use colors,
draw graphs and export its output to different formats such as CSV or HTML.


%prep
%setup -q
mkdir build


%build
pushd build
%cmake \
    -DSYSCONFDIR=%{_sysconfdir} \
    -DXDG_CONFIG_DIR=%{_sysconfdir}/xdg \
    -DNLS_ENABLED=0 ..
popd


%install
pushd build
  %make_install
popd
rm -rf $RPM_BUILD_DIR/usr/share/doc/dfc

%files
%config(noreplace) %{_sysconfdir}/xdg/%{name}
%doc AUTHORS CHANGELOG HACKING README TRANSLATORS LICENSE
%license LICENSE
%{_bindir}/dfc
%{_mandir}/man1/dfc.1.*


%changelog
* Wed Oct 31 2018 L.L.Robinson <baggypants@fedoraproject.org> - 3.0.5-10
- new buildrequires for copr/koji

* Tue Aug 23 2016 L.L.Robinson <baggypants@fedoraproject.org> - 3.0.5-9
- fixed docs directive

* Sat Feb 07 2015 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-8
- Rebuild/Revbump

* Wed Feb 04 2015 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-7
- Use license macro

* Wed Dec 31 2014 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-6
- Misc:
- Clean-up, tidy-up, extra pick-ups from auto-br-rpmbuild

* Fri Dec 26 2014 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-5
- Cleanups.

* Sun Nov 16 2014 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-4
- Don't build solitary french man page.

* Wed Nov 05 2014 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-3
- Minor cleanup/formatting/sorting.

* Wed Oct 29 2014 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-2
- Fix build in F19

* Wed Oct 29 2014 Ray Griffin <rorgoroth@openmailbox.org> - 3.0.5-1
- Initial package, test out copr.
