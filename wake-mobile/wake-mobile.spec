%global debug_package %{nil}

Name:           wake-mobile
Version:        1.7
Release:        4%{?dist}
Summary:        systemd timer hwclock alarm 

License:        GPLv2
URL:            https://gitlab.gnome.org/kailueke/wake-mobile/
Source0:        https://gitlab.gnome.org/kailueke/wake-mobile/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc desktop-file-utils systemd
Requires:       python3-psutil

%description
Proof-of-concept alarm app that uses systemd timers to wake up the system


%prep
%autosetup


%build
%make_build


%install
#rm -rf $RPM_BUILD_ROOT
install -D  -m 644 system-wake-up.service %{buildroot}/%{_unitdir}/system-wake-up.service
install -D  -m 644 system-wake-up.timer %{buildroot}/%{_unitdir}/system-wake-up.timer
install -D  -m 4755 set-user-alarm %{buildroot}/%{_bindir}/set-user-alarm
install -D  -m 755 wake-mobile %{buildroot}/%{_bindir}/wake-mobile
install -D  -m 644 wake-mobile.ui %{buildroot}/%{_datadir}/wake-mobile/wake-mobile.ui
install -D  -m 644 org.gnome.gitlab.kailueke.WakeMobile.desktop %{buildroot}/%{_datadir}/applications/org.gnome.gitlab.kailueke.WakeMobile.desktop
install -D  -m 644 org.gnome.gitlab.kailueke.WakeMobile.service %{buildroot}/%{_datadir}/dbus-1/services/org.gnome.gitlab.kailueke.WakeMobile.service                             
install -D  -m 644 org.gnome.gitlab.kailueke.WakeMobile.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/org.gnome.gitlab.kailueke.WakeMobile.svg
desktop-file-validate %{buildroot}/%{_datadir}/applications/org.gnome.gitlab.kailueke.WakeMobile.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/set-user-alarm
%{_bindir}/wake-mobile
%{_unitdir}/system-wake-up.service
%{_unitdir}/system-wake-up.timer
%{_datadir}/applications/org.gnome.gitlab.kailueke.WakeMobile.desktop
%{_datadir}/dbus-1/services/org.gnome.gitlab.kailueke.WakeMobile.service
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.gitlab.kailueke.WakeMobile.svg
%{_datadir}/wake-mobile/wake-mobile.ui




%changelog
* Sat Apr 17 2021 baggypants - 1.7-4
- discovered systemd build requires are a thing

* Sat Apr 17 2021 baggypants - 1.7-3
- discovered systemd macros are a thing

* Sat Apr 17 2021 baggypants - 1.7-2
- Added a required python package

* Sat Apr 17 2021 baggypants
- Initial Build
