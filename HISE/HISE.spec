%global commit0 e5e2d1f95fe5b38c9f37ceaf6e38a4d34b692d08
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
Name:           HISE
Version:        638git
Release:        1.%{shortcommit0}%{?dist}
Summary:        Sampler

License:        GPLv3


URL:            http://hise.audio/
Source0:        https://github.com/christophhart/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires:  llvm clang freetype-devel libX11-devel libXinerama-devel libXrandr-devel libXcursor-devel
BuildRequires:  alsa-lib-devel freeglut-devel libXcomposite-devel libcurl-devel jack-audio-connection-kit-devel
#Requires:       

%description
a sampler

%prep
%autosetup -n %{name}-%{commit0}


%build
cd projects/standalone/TravisCI/LinuxMakefile/
#make_build
make

%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Sat Dec 31 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
