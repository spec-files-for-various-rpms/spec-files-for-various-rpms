Name:           angelfish
Version:        21.08
Release:        4%{?dist}
Summary:        Plasma Mobile minimal webbrowser

License:        MIT and GPLv2+ and LGPLv2 and LGLPv2+
URL:            https://invent.kde.org/plasma-mobile/%{name}
Source0:        %{url}/-/archive/v%{version}/%{name}-v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  desktop-file-utils
BuildRequires:  cmake gcc-c++ extra-cmake-modules qconf qt5-qtbase-devel qt5-qt3d-devel
BuildRequires:  qt5-qtquickcontrols2-devel qt5-qtsvg-devel qt5-qtwebengine-devel
BuildRequires:  kf5-kirigami2-devel kf5-kconfig-devel kf5-purpose-devel
BuildRequires:  kf5-ki18n-devel kf5-kcoreaddons kf5-kdbusaddons-devel
BuildRequires:  kf5-knotifications-devel kf5-kservice-devel kf5-kwindowsystem-devel
BuildRequires:  qt5-qtfeedback-devel
Requires:       qt5-qtwayland kf5-kirigami2 qt5-qtfeedback

%description
Experimental web browser designed to be used on small mobile devices and integrate well in Plasma workspaces

%prep
%autosetup -n %{name}-v%{version}


%build
%cmake .
%cmake_build


%install
%cmake_install
desktop-file-validate %{buildroot}/%{_datadir}/applications/org.kde.angelfish.desktop

%files
%license LICENSES/{MIT,GPL-2.0-or-later,LGPL-2.0-only,LGPL-2.0-or-later}.txt
%doc README.md
%{_bindir}/%{name}
%{_bindir}/%{name}-webapp
%{_datadir}/applications/org.kde.angelfish.desktop
%{_datadir}/config.kcfg/angelfishsettings.kcfg
%{_datadir}/icons/hicolor/scalable/apps/org.kde.angelfish.svg
%{_datadir}/knotifications5/angelfish.notifyrc
%{_datadir}/metainfo/org.kde.angelfish.metainfo.xml

%changelog
* Tue Oct 12 2021 Baggypants <junk@therobinsonfamily.net> - 21.08-4
- added qtfeedback as dep

* Tue Oct 12 2021 Baggypants <junk@therobinsonfamily.net> - 21.08-3
- updated to 21.08

