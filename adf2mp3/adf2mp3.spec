Name:           adf2mp3
Version:        1.0
Release:        1%{?dist}
Summary:        Small tool for converting GTA:Vice city radio files

License:        GFDL
URL:            http://www.gtamodding.com/wiki/ADF
Source0:        %{name}-%{version}.tgz

%description
Small tool for converting GTA:Vice city radio files to mp3 taken from gtamodding wiki

%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
%make_install PREFIX=%{_prefix}


%files
%{_bindir}/adf2mp3


%changelog
* Wed Feb  3 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
