#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	// check command-line arguments
	if (argc != 3)
	{
		printf("Usage: adftool <inputFile> <outputFile>\n");
		return 1;
	}
	
	// open input file
	FILE *inputFile = fopen(argv[1], "rb");
	if (!inputFile)
	{
		printf("Failed to open %s!\n", argv[1]);
		return 1;
	}
	
	// open output file
	FILE *outputFile = fopen(argv[2], "wb");
	if (!outputFile)
	{
		printf("Failed to open %s!\n", argv[2]);
		fclose(inputFile);
		return 1;
	}
	
	// allocate buffer (16 MB)
	const size_t bufferSize = 1024 * 1024 * 16;
	char *buffer = (char *)malloc(bufferSize);
	if (!buffer)
	{
		printf("Failed to allocate buffer!\n");
		fclose(inputFile);
		fclose(outputFile);
		return 1;
	}
	
	// read input file into buffer block by block
	size_t size;
	while ((size = fread(buffer, 1, bufferSize, inputFile)) > 0)
	{
		// xor buffer against 0x22
		char *iterator = buffer;
		for (size_t i = 0; i < size; i++)
		{
			*iterator++ ^= 0x22;
		}
		
		// write buffer to output file
		fwrite(buffer, 1, size, outputFile);
	}
	
	// deallocate buffer and close files
	free(buffer);
	fclose(inputFile);
	fclose(outputFile);
	
	return 0;
}

