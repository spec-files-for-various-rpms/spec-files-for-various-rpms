Name:           drumgizmo
Version:        0.9.11
Release:        1%{?dist}
Summary:        Open Source Drum Machine

License:        GPLv3
URL:            http://www.drumgizmo.org
Source0:        http://www.drumgizmo.org/releases/%{name}-%{version}/%{name}-%{version}.tar.gz   

BuildRequires:  lv2-devel libsndfile-devel expat-devel zita-convolver-devel libX11-devel
Requires:       lv2

%description
DrumGizmo is an open source, multichannel, multilayered, cross-platform drum 
plugin and stand-alone application. It enables you to compose drums in midi and 
mix them with a multichannel approach. It is comparable to that of mixing a 
real drumkit that has been recorded with a multimic setup.

%prep
%autosetup 


%build
%configure --enable-lv2
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license COPYING
%doc 

%changelog
* Sun Oct 30 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
