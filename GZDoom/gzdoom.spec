Name:           gzdoom
Version:        g2.2pre
Release:        1%{?dist}
Summary:        GZDoom engine

License:        Awful
URL:            https://github.com/coelckers/%{name}
Source0:        https://github.com/coelckers/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires: gcc-c++ make cmake SDL2-devel git zlib-devel bzip2-devel 
BuildRequires: libjpeg-turbo-devel fluidsynth-devel game-music-emu-devel openal-soft-devel 
BuildRequires: libmpg123-devel libsndfile-devel wildmidi-devel gtk3-devel timidity++ nasm tar 
BuildRequires: chrpath


%description
A doom engine

%global debug_package %{nil}

%prep
%setup -q

%build
%cmake -DCMAKE_BUILD_TYPE=Release
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
mkdir -pv $RPM_BUILD_ROOT/usr/games/gzdoom
cp -v {gzdoom,gzdoom.pk3,lights.pk3,brightmaps.pk3} $RPM_BUILD_ROOT/usr/games/gzdoom


%files
%doc
/usr/games/gzdoom


%changelog
* Sat Apr 30 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
