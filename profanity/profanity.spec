Name:           profanity
Version:        0.6.0
Release:        3%{?dist}
Summary:        A console based XMPP client

License:        GPLv3+
URL:            http://profanity.im.github.io/
Source0:        http://profanity.im/%{name}-%{version}.tar.gz

BuildRequires:  libstrophe-devel ncurses-devel glib2-devel libcurl-devel
BuildRequires:  readline-devel libuuid-devel libnotify-devel libX11-devel
BuildRequires:  libotr-devel gpgme-devel gcc
Requires:       libstrophe%{?_isa} >= 0:0.9.1

%description
A console based XMPP client inspired by Irssi that supports OTR, PGP, MUC
chat room support, Desktop notifications and Roster management.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license COPYING 
%doc 
%{_bindir}/profanity
%{_mandir}/man1/profanity.1.*
%{_datarootdir}/profanity
%{_libdir}/libprofanity.*

%files devel
%{_includedir}/profapi.h

%changelog
* Mon May 06 2019 L.L.Robinson <baggypants@fedoraproject.org> - 0.6.0-3
- needs a compiler

* Mon May 06 2019 L.L.Robinson <baggypants@fedoraproject.org> - 0.6.0-2
- needs a compiler

* Mon May 06 2019 L.L.Robinson <baggypants@fedoraproject.org> - 0.6.0-1
- new version

* Sun Oct 16 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-4
- added support for other things

* Sat Oct 15 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-3
- Updated summary and requires

* Sat Oct 15 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-2
- added new files and split out header file

* Sat Oct 15 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-1
- new version

* Mon Aug 22 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
