%global debug_package %{nil}
%global commit0 e62a2ae027c459ddfcea354f074cb0f5c2be356d
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name:           lord-almightys-modern-bible
Version:        %{shortcommit0}
Release:        1%{?dist}
Summary:	Lord Almighty Modern Bible        

License:        GPLv3
URL:            https://gitlab.com/The3DmaN/lord-almightys-modern-bible/
Source0:        https://gitlab.com/The3DmaN/%{name}/-/archive/%{commit0}/%{name}-%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires:  qt5-qtbase-devel qt5-qtquickcontrols2-devel qt5-qtsvg-devel qt5-qtwebengine-devel
BuildRequires:	desktop-file-utils
Requires:	qt5-qtwayland

%description
A bible app writtenin qtquick

%prep
%autosetup -n %{name}-%{commit0}


%build
qmake-qt5 USE_QRCODE=1 USE_UPNP=1
%make_build


%install
install -Dm755 LAMB %{buildroot}/%{_bindir}/lord-almightys-modern-bible
install -Dm644 images/LAMB.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/LAMB.svg
install -Dm644 LAMB.desktop %{buildroot}/%{_datadir}/applications/LAMB.desktop

desktop-file-validate %{buildroot}/%{_datadir}/applications/LAMB.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/lord-almightys-modern-bible
%{_datadir}/icons/hicolor/scalable/apps/LAMB.svg
%{_datadir}/applications/LAMB.desktop



%changelog
* Tue Oct 12 2021 Baggypants <junk@therobinsonfamily.net>
- 
