#%global commit0 1d434add503894c63f3e8bf1496d591b09854fb0
#%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
Name:           zdoom
Version:        2.8.1
Release:        4%{?dist}
#Release: 2git%{shortcommit0}%{?dist}
Summary:        ZDoom engine

License:        Awful
URL:            http://zdoom.org
Source0:        https://github.com/rheit/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
#Source0:	https://github.com/rheit/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz
Source1:        http://www.fmod.org/download/fmodex/api/Linux/fmodapi42416linux64.tar.gz
Source2:        http://www.fmod.org/download/fmodex/api/Linux/fmodapi42416linux.tar.gz

BuildRequires:  gcc-c++ make zlib-devel SDL-devel SDL2-devel libjpeg-turbo-devel
BuildRequires: nasm tar bzip2-devel gtk2-devel cmake git fluidsynth-devel game-music-emu-devel
BuildRequires: openal-soft-devel timidity++ mesa-libGL-devel glew-devel


%description
A doom engine


%prep
%setup -q -a 1



%build

%cmake  -DCMAKE_BUILD_TYPE=Release -DSYSTEM_LIBS=ON -DSHARE_DIR="%{_datadir}/%{name}"
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p %{buildroot}%{_libdir}/%{name}
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_bindir}
install -m 0644 zdoom.pk3 %{buildroot}%{_datadir}/%{name}
install -m 0755 {lzma/liblzma.so,game-music-emu/gme/libgme.so,gdtoa/libgdtoa.so,dumb/libdumb.so,fmodapi42416linux64/api/lib/libfmod*.so,output_sdl/liboutput_sdl.so} %{buildroot}%{_libdir}/%{name}
install -m 0755 zdoom %{buildroot}%{_bindir}

%files
%doc
%{_bindir}/zdoom
%{_libdir}/zdoom/
%{_datadir}/zdoom/




%changelog
* Fri Aug 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 2.8.1-4
- added liblzma and libgme

* Fri Aug 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 2.8.1-3
- added gdtoa/libgdtoa.so

* Fri Aug 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 2.8.1-2
- added libdumb.so

* Sat Apr 30 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
