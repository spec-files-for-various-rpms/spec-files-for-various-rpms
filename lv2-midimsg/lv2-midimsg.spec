Name:           lv2-midimsg
Version:        0.0.1
Release:        1%{?dist}
Summary:        Convert Midi to plucin controllers

License:        GPLv2
URL:            https://objectivewave.wordpress.com/midimsg-lv2/
Source0:        http://github.com/blablack/midimsg-lv2/archive/v%{version}.tar.gz#/midimsg-lv2-0.0.1.tar.gz

BuildRequires:  python lv2-devel
Requires:       lv2

%description
An LV2 plugin to convert midi to control signals for other plugins

%prep
%setup -q -n midimsg-lv2-0.0.1


%build
./waf configure --lv2dir=%{buildroot}%{_libdir}/lv2/ 
./waf

%install
rm -rf $RPM_BUILD_ROOT
./waf install

%files
%doc README.md COPYING
%{_libdir}/lv2/midimsg.lv2




%changelog
* Mon Nov 16 2015 L.L.Robinson <baggypants@fedoraproject.org>
- 
