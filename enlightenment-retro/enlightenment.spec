Name:           enlightenment-retro
Version:        0.17.6
Release:        3%{?dist}
License:        BSD
Summary:        Enlightenment window manager
Url:            http://enlightenment.org
Source:         http://download.enlightenment.org/releases/enlightenment-%{version}.tar.bz2
Patch0:		enlightenment-0.17.6-systemd-suspend-hibernate.patch

BuildRequires:  alsa-lib-devel
BuildRequires:  dbus-devel 
BuildRequires:  desktop-file-utils
BuildRequires:  doxygen
BuildRequires:  moksha-e_dbus-devel  >= 1.7.10
BuildRequires:	efl-devel
BuildRequires:  libXext-devel 
BuildRequires:  libeina-devel 
BuildRequires:  pam-devel
BuildRequires:  xcb-util-keysyms-devel
Requires:       %{name}-data = %{version}-%{release}
Requires:       emotion >= 1.7.10
Requires:       elementary >= 1.7.10
Requires:       ethumb >= 1.7.10
Requires:       evas-generic-loaders >= 1.7.10
Requires:       redhat-menus
Provides:       firstboot(windowmanager) = enlightenment
Conflicts:	enlightenment
Conflicts:	moksha-desktop

%description
Enlightenment window manager is a lean, fast, modular and very extensible window 
manager for X11 and Linux. It is classed as a "desktop shell" providing the 
things you need to operate your desktop (or laptop), but is not a whole '
application suite. This covered launching applications, managing their windows 
and doing other system tasks like suspending, reboots, managing files etc. 

%package        data
Summary:        Enlightenment data files
Requires:       %{name} = %{version}-%{release}
BuildArch:      noarch

%description data
Contains data files for Enlightenment

%package        devel
Summary:        Enlightenment headers, documentation and test programs

%description devel
Headers,  test programs and documentation for enlightenment

%prep
%setup -q -n enlightenment-%{version}
%patch0 -p1 -b .systemd

%build
%configure --disable-static --disable-rpath
make %{?_smp_mflags} V=1

%install
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -delete

%find_lang enlightenment
desktop-file-validate %{buildroot}/%{_datadir}/applications/*.desktop

%files
%doc AUTHORS COPYING README NEWS
%{_sysconfdir}/xdg/menus/enlightenment.menu
%{_sysconfdir}/enlightenment/sysactions.conf
%{_bindir}/enlightenment
%{_bindir}/enlightenment_filemanager
%{_bindir}/enlightenment_imc
%{_bindir}/enlightenment_open
%{_bindir}/enlightenment_remote
%{_bindir}/enlightenment_start
%{_libdir}/enlightenment

%files data -f enlightenment.lang
%{_datadir}/xsessions/enlightenment.desktop
%{_datadir}/enlightenment
%{_datadir}/applications/*.desktop

%files devel
%{_libdir}/pkgconfig/*.pc
%{_includedir}/enlightenment

%changelog
* Wed Aug 26 2015 L.L.Robinson <baggypants@fedoraproject.org> - 0.17.6-3
- rebuildofretro

* Fri Dec 12 2014 Tom Callaway <spot@fedoraproject.org> - 0.17.6-2
- use systemctl calls to suspend/hibernate

* Thu Oct 23 2014 Tom Callaway <spot@fedoraproject.org> - 0.17.6-1
- update to 0.17.6

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Nov 12 2013 Dan Mashal <dan.mashal@fedoraproject.org> 0.17.5-2
- Add emotion-devel to BRs

* Thu Nov 07 2013 Dan Mashal <dan.mashal@fedoraproject.org> 0.17.5-1
- Update to 0.17.5

* Mon Oct 07 2013 Dan Mashal <dan.mashal@fedoraproject.org> 0.17.4-4
- Add hard runtime requirements so one package can install the entire stack.

* Sun Oct 06 2013 Dan Mashal <dan.mashal@fedoraproejct.org> 0.17.4-3
- Add versioned build deps.

* Sun Oct 06 2013 Dan Mashal <dan.mashal@fedoraproejct.org> 0.17.4-2
- Update spec as per package review #1014619

* Tue Sep 24 2013 Dan Mashal <dan.mashal@fedoraproject.org> 0.17.4-1
- Update to 0.17.4
- Clean up spec file
- Update license from MIT to BSD

* Wed Jan 02 2013 Rahul Sundaram <sundaram@fedoraproject.org> - 0.17.0-1
- initial spec
