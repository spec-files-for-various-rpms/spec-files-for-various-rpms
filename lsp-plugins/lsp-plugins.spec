Name:           lsp-plugins
Version:        1.0.20
Release:        1%{?dist}
Summary:        Linux Studio Plugins Project

License:        Unkoiwn
URL:            http://lsp-plug.in/
Source0:        https://sourceforge.net/code-snapshots/svn/l/ls/lsp-plugins/svn/lsp-plugins-svn-5-trunk.zip

BuildRequires:  jack-audio-connection-kit-devel lv2-devel libsndfile-devel cairo-devel gtk2-devel
#Requires:       

%description
Plugins 

%prep
%autosetup -n lsp-plugins-svn-5-trunk


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
#%license add-license-file-here
%doc add-docs-here



%changelog
* Mon Feb 20 2017 L.L.Robinson <baggypants@fedoraproject.org>
- 
