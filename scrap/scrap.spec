%global debug_package %{nil}
Name:           scrap
Version:        release
Release:        3%{?dist}
Summary:        Robot rouguelike where you scavenge parts to become better

License:        Available source
URL:            https://web.archive.org/web/20130524032130/http://www.math.leidenuniv.nl/~mommen/scrap/
Source0:        https://web.archive.org/web/20130524032130/http://www.math.leidenuniv.nl/~mommen/scrap/x10drl1_1/scrap.tar.gz
Patch0:		https://gitlab.com/spec-files-for-various-rpms/spec-files-for-various-rpms/raw/master/scrap/scrap.patch

BuildRequires:	ncurses-devel gcc-c++

%description


%prep
%setup -c
%patch0 -p1

%build
%make_build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install scrap $RPM_BUILD_ROOT%{_bindir}/

%files
%doc manual.txt
%{_bindir}/scrap


%changelog
* Wed Oct 31 2018 L.L.Robinson <baggypants@fedoraproject.org> - release-3
- new mock/koji build requrements

* Fri Oct 12 2018 L.L.Robinson <baggypants@fedoraproject.org> - release-2
- remove debug requirements

* Tue Aug 23 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
