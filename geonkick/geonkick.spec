%global gittag v%{version}
%global commit dff55cc09836b7926d1a355157ce6a1f80045b36

Name:           geonkick
Version:        1.0
Release:        1%{?dist}
Summary:        software percussion synthesizer

License:        GPL3
URL:            https://gitlab.com/iurie/geonkick
Source0:        https://gitlab.com/iurie/%{name}/-/archive/%{gittag}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++, qt5-devel, libsndfile-devel, jack-audio-connection-kit-devel

%description
Geonkick is a synthesizer that can synthesize elements of percussion. The most basic examples are: kick drums, snares, hit-hats, shakers, claps, steaks.

%prep
%autosetup -n %{name}-%{gittag}-%{commit}


%build
%cmake . "-DCMAKE_INSTALL_PREFIX=%{_prefix}"
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license LICENSE
%doc README.md
/usr/lib/debug/usr/local/bin/geonkick-1.0-1.fc29.x86_64.debug
/usr/local/bin/geonkick
/usr/share/applications/geonkick.desktop
/usr/share/icons/hicolor/128x128/apps/geonkick.png
/usr/share/icons/hicolor/16x16/apps/geonkick.png
/usr/share/icons/hicolor/22x22/apps/geonkick.png
/usr/share/icons/hicolor/24x24/apps/geonkick.png
/usr/share/icons/hicolor/32x32/apps/geonkick.png
/usr/share/icons/hicolor/48x48/apps/geonkick.png
/usr/share/icons/hicolor/64x64/apps/geonkick.png
/usr/share/icons/hicolor/scalable/apps/geonkick.svg
/usr/share/mime/packages/geonkick.xml


%changelog
* Fri Jan 11 2019 L.L.Robinson <baggypants@fedoraproject.org>
- 
