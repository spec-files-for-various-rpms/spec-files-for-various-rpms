Name:           buzztrax
Version:        0.10.2
Release:        5%{?dist}
Summary:        Buzztrax is a music composer similar to tracker applications.

License:        LGPL2.1
URL:            http://www.buzztrax.org 
Source0:        http://files.buzztrax.org/releases/%{name}-%{version}.tar.gz
BuildRequires:  gstreamer1-devel gstreamer1-plugins-base-devel libxml2-devel gsf-sharp
BuildRequires:  clutter-gtk-devel gtk+-devel gettext-devel gtk-doc
BuildRequires:  intltool libtool gstreamer1-plugins-good desktop-file-utils orc-compiler
BuildRequires:  alsa-lib-devel libgudev-devel fluidsynth-devel goffice-devel
#Requires:       

%description

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries and header files for

%prep

%autosetup


%build
%configure --enable_dllwrapper=no
%make_build CFLAGS="%{optflags} -Wno-error"



%install
rm -rf $RPM_BUILD_ROOT
%make_install
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}-edit.desktop
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}-songio-buzz.desktop


%files
%license COPYING
%doc README.md
%{_datadir}/gtk-doc/html/buzztrax-*
%{_bindir}/buzztrax-cmd
%{_bindir}/buzztrax-edit
/usr/lib/buzztrax-songio/*
%{_libdir}/buzztrax
%{_libdir}/gstreamer-1.0/libbuzztrax*
%{_libdir}/gstreamer-1.0/libgstbml.*
%{_libdir}/gstreamer-1.0/libgstsidsyn.*
%{_libdir}/gstreamer-1.0/libgstfluidsynth.*
%{_libdir}/libbml.*
%{_libdir}/libbuzztrax-core.*
%{_libdir}/libbuzztrax-gst.*
%{_libdir}/libbuzztrax-ic.*
%{_datadir}/buzztrax
%{_datadir}/icons/gnome/*/apps/%{name}*.png
%{_datadir}/icons/gnome/*/apps/%{name}*.svg
%{_datadir}/icons/hicolor/*/apps/%{name}*.png
%{_datadir}/icons/hicolor/*/apps/%{name}*.svg
%{_datadir}/locale/*/LC_MESSAGES/%{name}-%{version}.mo
%{_datadir}/mime/packages/%{name}*.xml
%{_datadir}/GConf/gsettings/buzztrax.convert
%{_datadir}/gstreamer-1.0/presets/GstBtEBeats.prs
%{_datadir}/gstreamer-1.0/presets/GstBtSimSyn.prs
%{_datadir}/applications/%{name}-edit.desktop
%{_datadir}/applications/%{name}-songio-buzz.desktop
%{_datadir}/appdata/buzztrax.appdata.xml
%{_datadir}/glib-2.0/schemas/org.buzztrax.gschema.xml

%post
/bin/touch --no-create %{_datadir}/icons/gnome &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /bin/touch --no-create %{_datadir}/icons/gnome &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :

%files devel
%{_includedir}/libbml
%{_includedir}/libbuzztrax-core
%{_includedir}/libbuzztrax-gst
%{_includedir}/libbuzztrax-ic
%{_libdir}/pkgconfig/libbml.pc
%{_libdir}/pkgconfig/libbuzztrax-core.pc
%{_libdir}/pkgconfig/libbuzztrax-gst.pc
%{_libdir}/pkgconfig/libbuzztrax-ic.pc

%changelog
* Sun Oct 23 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.10.2-5
- Add more functionality requires

* Sun Oct 23 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.10.2-4
- changed source

* Fri Oct 14 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.10.2-3
- added orc dep

* Fri Oct 14 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.10.2-2
- update spec to update icons on install

* Fri Oct 14 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
