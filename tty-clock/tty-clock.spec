Name:		tty-clock
Version:	git
Release:        3.20150409git516afbf9%{?dist}
Summary:	Console Clock

Group:		Applications/System
License:	Public Domain
URL:		https://github.com/xorg62/tty-clock
Source0:        tty-clock-516afbf9.tar

BuildRequires:	ncurses-devel
Requires:	ncurses

%description
Console Clock

%prep
%setup -q -n tty-clock


%build
make -j1 %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}


%files
%doc



%changelog
* Thu Apr 09 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-3.20150409git516afbf9
- Update to git: 516afbf9

* Sun Feb 08 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-2.20150208git4e6b4cae
- Update to git: 4e6b4cae






