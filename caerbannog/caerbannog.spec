%global debug_package %{nil}

Name:           caerbannog
Version:        0.3
Release:        1%{?dist}
Summary:        A GTK3 interface to your password-store, optimized for mobile/handheld devices.

License:        GPLv3
URL:            https://git.sr.ht/~craftyguy/%{name}
Source0:        %{url}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  meson gcc libhandy1-devel desktop-file-utils
Requires:       python3 python3-anytree python3-fuzzyfinder python3-gobject python3-gpg
Requires:	pinentry-gnome3

%description
Password store which fuzzy search, pinentry, adaptive ui

%prep
%autosetup


%build
%meson
%meson_build

%install
%meson_install
desktop-file-validate %{buildroot}/%{_datadir}/applications/caerbannog.desktop

%files
%license COPYING
%{_bindir}/caerbannog
%{_datadir}/applications/caerbannog.desktop
%{_datadir}/caerbannog/
%{_datadir}/icons/hicolor/scalable/apps/net.craftyguy.caerbannog.svg
#doc README.md



%changelog
* Sun Apr 18 2021 Baggypants <junk@therobinsonfamily.net>
- 
