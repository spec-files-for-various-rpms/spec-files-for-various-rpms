%global commit0 9c6a2741bea81d8a9f799678aadfe194e48b9be6
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global dpfcommit0 cda831ffcd5f4fe6138e86e5fab70d81f4cf3a18 
%global dpfshortcommit0 %(c=%{dpfcommit0}; echo ${c:0:7})
%global debug_package %{nil}

Summary:       Acid sounds synthesizer
Name:          Nekobi-dssi
Version:       0.1.7
Release:       2git%{shortcommit0}%{?dist}
License:       GPLv2+
Group:         Applications/Multimedia
URL:           https://github.com/DISTRHO/Nekobi/
Source0:       https://github.com/DISTRHO/Nekobi/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

Source1:       nekobee.desktop
# Derived from extra/knob.png
Source2:       nekobee.png
Source3:       https://github.com/DISTRHO/DPF/archive/%{dpfcommit0}.tar.gz#/dpf-%{dpfshortcommit0}.tar.gz

BuildRequires: alsa-lib-devel
BuildRequires: desktop-file-utils
BuildRequires: dssi-devel
BuildRequires: gtk2-devel
BuildRequires: liblo-devel


%description
This package provides the nekobee plugin, which is suitable for recreating
those squelchy acid sounds.


%package -n lv2-Nekobi
Summary:       Nekobee lv2 plugin
Group:         Applications/Multimedia
Requires:      lv2%{?_isa} 

%description -n lv2-Nekobi
This package provides the nekobee plugin, which is suitable for recreating
those squelchy acid sounds.


%prep
%setup -n Nekobi-%{commit0}
%setup -T -D -a 3 -n Nekobi-%{commit0}
rmdir dpf
mv DPF-%{dpfcommit0} dpf
%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

# Make a symlink for easy access
mkdir -p $RPM_BUILD_ROOT%{_bindir}
ln -s jack-dssi-host $RPM_BUILD_ROOT%{_bindir}/Nekobi


# Desktop file
mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
desktop-file-install                              \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
  %{SOURCE1}

# Icon
mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/apps
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/48x48/apps/nekobee.png
# Install lv2
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/lv2
cp -r bin/Nekobi.lv2  ${RPM_BUILD_ROOT}%{_libdir}/lv2
#install dssi
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/dssi
install bin/Nekobi-dssi.so ${RPM_BUILD_ROOT}%{_libdir}/dssi
cp -r bin/Nekobi-dssi ${RPM_BUILD_ROOT}%{_libdir}/dssi


%clean
rm -rf $RPM_BUILD_ROOT

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root,-)
%doc README.md 
%license LICENSE
%{_bindir}/Nekobi
%{_libdir}/dssi/Nekobi-dssi/
%{_libdir}/dssi/Nekobi-dssi.so
%{_datadir}/applications/nekobee.desktop
%{_datadir}/icons/hicolor/48x48/apps/nekobee.png

%files -n lv2-Nekobi
%{_libdir}/lv2/Nekobi.lv2
%changelog
* Tue Oct 25 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.1.7-2git9c6a274
- updated to new code, split lv2 into new package

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.1.7-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.1.7-6
- Rebuild for new libpng

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Jul 20 2010 Orcan Ogetbil <oget[DOT]fedora[AT]gmail[DOT]com> - 0.1.7-4
- Rebuild against new liblo-0.26

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jun 04 2009 Orcan Ogetbil <oget[DOT]fedora[AT]gmail[DOT]com> - 0.1.7-2
- Add icon cache scriptlet

* Sat May 30 2009 Orcan Ogetbil <oget[DOT]fedora[AT]gmail[DOT]com> - 0.1.7-1
- Initial build
