Name:           avant-window-navigator
Version:        0.4.2
Release:        1%{?dist}
Summary:        A Dock like bar that sits at the bottom of the screen

License:        GPL2
URL:            https://launchpad.net/awn
Source0:        https://launchpad.net/awn/0.4/0.4.2/+download/%{name}-%{version}.tar.gz

BuildRequires:  python-devel pygtk2-devel vala-compat pyxdg intltool
#Requires:       

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Wed Nov 30 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
