Name:           sequencer64
Version:        0.96.1
Release:        1%{?dist}
Summary:        MIDI sequencer

License:        GPL
URL:            https://github.com/ahlstromcj/sequencer64
Source0:        https://github.com/ahlstromcj/sequencer64-packages/blob/master/latest/sequencer64-master-rtmidi-0.96.0.tar.xz

BuildRequires:  jack-audio-connection-kit-devel
#Requires:       

%description
Sequencer64 is a reboot of seq24, extending it with many new features.
The heart of seq24 remains intact.

%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
#license add-license-file-here
#doc add-docs-here



%changelog
* Mon Nov 19 2018 L.L.Robinson <baggypants@fedoraproject.org>
- 
