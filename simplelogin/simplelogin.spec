%global commit fa62692b24e2b13e59f33ca5bd64f11729440713
%global shortcommit %(c=%{commit}; echo ${c:0:7})
Name:           simplelogin
Version:        0.0.1.20200510
Release:        1%{?dist}
Summary:        Super simple sans display display desktop manager mockup mitigating mir

License:        GPLv2
URL:            https://invent.kde.org/bshah/%{name}
Source0:        %{url}/-/archive/%{commit}/%{name}-%{commit}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  extra-cmake-modules
BuildRequires:  qconf
BuildRequires:  qt5-qtbase-devel

%description
This app goes through PAM with logind, forks into the user specified, like a login manager would do

%prep
%autosetup -n %{name}-%{commit}


%build
%cmake
%cmake_build


%install
%cmake_install


%files 
%license LICENSE
%doc README



%changelog
* Mon May 10 2021 Baggypants <junk@therobinsonfamily.net>
- 
