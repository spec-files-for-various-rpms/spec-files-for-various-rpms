Name:		cmatrix
Version:	1.2a
Release:	3%{?dist}
Summary:	console matrix

Group:		Applications
License:	GPLv2
URL:		http://www.asty.org/cmatrix/
Source0:	http://www.asty.org/cmatrix/dist/cmatrix-1.2a.tar.gz

BuildRequires:	ncurses-devel, libX11-devel, kbd gcc-c++
Requires:	bash

%description


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
%make_install


%files
%doc

%{_bindir}/%{name}
%{_mandir}/man1/cmatrix.1*


%changelog
* Wed Oct 31 2018 L.L.Robinson <baggypants@fedoraproject.org> - 1.2a-3
- New build requirements for copr/koji

* Mon Aug 10 2015 Leon L. Robinson 1.2a-1
- initial build
