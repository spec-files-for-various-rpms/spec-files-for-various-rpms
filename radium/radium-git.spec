Name:           radium-git
Version:        3.4.4
Release:        2.20151205gite95a9fe5%{?dist}
Summary:        tracker

License:        dubious
URL:            http://users.notam02.no/~kjetism/radium/
Source0:        radium-e95a9fe5.tar

BuildRequires:	libXaw-devel python-devel alsa-lib-devel jack-audio-connection-kit-devel
BuildRequires:	libsamplerate-devel liblrdf-devel libsndfile-devel ladspa-devel ladspa-calf-plugins
BuildRequires:	binutils-devel libtool-ltdl libtool tk-devel libogg-devel 
BuildRequires:	libvorbis-devel speex-devel fftw-devel guile cmake libxkbfile-devel 
BuildRequires:	xorg-x11-util-macros qt-devel
Requires:       bash

%description


%prep
%setup -q -n radium


%build
#make %{?_smp_mflags}
./make_and_run_linux.sh


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%doc



%changelog
* Sat Dec 05 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 3.4.4-2.20151205gite95a9fe5
- Update to git: e95a9fe5

* Sat Dec  5 2015 L.L.Robinson <baggypants@fedoraproject.org>
-
