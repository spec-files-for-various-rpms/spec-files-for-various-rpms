%global commit 946c5dd80a6ebdb84c5986e52af460e23e74394d
%global shortcommit %(c=%{commit}; echo ${c:0:7})
Name:           corrosion
Version:        0.0.1.2020422
Release:        1%{?dist}
Summary:        Rust/CMake integration

License:        MIT
URL:            https://github.com/AndrewGaspar/corrosion
Source0:        %{url}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  cargo
BuildRequires:  rust-packaging

%description
Formerly known as cmake-cargo, it is a tool for integrating Rust into an existing CMake project;
capable of importing executables, static libraries, and dynamic libraries from a crate.


%prep
%autosetup -n %{name}-%{commit}

%cargo_prep




%build
%cmake . -DCORROSION_BUILD_TESTS=OFF -DCORROSION_DEV_MODE=OFF
%cmake_build


%install
%cmake_install


%files
%license LICENSE
%doc README.md



%changelog
* Thu Apr 22 2021 Baggypants <junk@therobinsonfamily.net>
- 
