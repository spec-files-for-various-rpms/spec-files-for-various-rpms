Summary: LinuxSampler Control Protocol library
Name: liblscp
Version: 0.5.7
Release: 1%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://qsampler.sourceforge.net/qsampler-index.html
Source0: http://download.linuxsampler.org/packages/liblscp-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Packager: Fernando Lopez-Lezcano
Distribution: Planet CCRMA
Vendor: Planet CCRMA

BuildRequires: automake, autoconf, libtool
BuildRequires: linuxsampler-devel

%description
LinuxSampler Control Protocol library

%package devel
Summary: LinuxSampler Control Protocol library developer resources
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
LinuxSampler Control Protocol library developer resources

%prep
%setup -q
if [ -f Makefile.cvs ]; then make -f Makefile.cvs; fi

%build
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{makeinstall}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_libdir}/liblscp.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/lscp
%{_libdir}/liblscp.so
%{_libdir}/liblscp.a
%exclude %{_libdir}/liblscp.la
%{_libdir}/pkgconfig/lscp.pc

%changelog
* Wed Jul 20 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.7-1
- new version

* Sat Nov  7 2009 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.5.6-1
- updated to 0.5.6

* Tue Jul  3 2007 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.5.5-1
- updated to 0.5.5

* Tue Jul  3 2007 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.5.3-1
- updated to 0.5.3

* Wed Dec  6 2006 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.4.2-1
- updated to 0.4.2

* Mon Jun 20 2006 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.3.3-1
- updated to 0.3.3

* Wed Jun 29 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.3.0-1
- updated to 0.3.0

* Thu May 26 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.9-1
- updated to 0.2.9

* Thu Jan 20 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.4-1
- initial build.
