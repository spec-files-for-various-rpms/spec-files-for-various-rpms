Name:		libgig
Version:	4.1.0
Release:	1%{?dist}
Summary:	libgig

Group:		Application/Media
License:	GPL
URL:		http://linuxsampler.org
Source0:	http://download.linuxsampler.org/packages/%{name}-%{version}.tar.bz2

BuildRequires:	intltool libsndfile-devel gcc-c++
#Requires:	

%description


%package devel
Summary:	development files for libgig
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description devel





%prep

%autosetup 


%build
%configure
%make_build


%install
%make_install


mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d/
echo "%{_libdir}/libgig" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/libgig.conf


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files devel
/usr/include/libgig/
%{_libdir}/pkgconfig/akai.pc
%{_libdir}/pkgconfig/gig.pc

%files
%doc

%{_bindir}/akaidump
%{_bindir}/akaiextract
%{_bindir}/dlsdump
%{_bindir}/gig2mono
%{_bindir}/gig2stereo
%{_bindir}/gigdump
%{_bindir}/gigextract
%{_bindir}/gigmerge
%{_bindir}/korg2gig
%{_bindir}/korgdump
%{_bindir}/rifftree
%{_bindir}/sf2dump
%{_bindir}/sf2extract
%{_sysconfdir}/ld.so.conf.d/libgig.conf
%{_libdir}/libgig/
%{_mandir}/man1/akaidump.1.gz
%{_mandir}/man1/akaiextract.1.gz
%{_mandir}/man1/dlsdump.1.gz
%{_mandir}/man1/gig2mono.1.gz
%{_mandir}/man1/gig2stereo.1.gz
%{_mandir}/man1/gigdump.1.gz
%{_mandir}/man1/gigextract.1.gz
%{_mandir}/man1/gigmerge.1.gz
%{_mandir}/man1/korg2gig.1.gz
%{_mandir}/man1/korgdump.1.gz
%{_mandir}/man1/rifftree.1.gz
%{_mandir}/man1/sf2dump.1.gz
%{_mandir}/man1/sf2extract.1.gz

%changelog
* Thu Oct 11 2018 L.L.Robinson <baggypants@fedoraproject.org> - 4.1.0-1
- new version

* Thu Jul 21 2016 L.L.Robinson <baggypants@fedoraproject.org> - 4.0.0-4
- fixed the missing shared lib error

* Wed Jul 20 2016 L.L.Robinson <baggypants@fedoraproject.org> - 4.0.0-3
- read up on how to package libs

* Tue Oct 06 2015 L.L.Robinson <baggypants@fedoraproject.org> - 4.0.0-2
- moved files to -devel


