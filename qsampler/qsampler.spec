%define	desktop_vendor planetccrma
%global commit0 ab4f537852a36885db873a2fb7d8d8e8c19344b6
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
#something strips the debug symbols, I don't know what
%global debug_package %{nil}


%if 0%{?build_svn}
%define rel 2.svn%{svnrev}
%else
%define rel 1
%endif

Summary: LinuxSampler GUI front-end
Name: qsampler
Version: 0.4.0
Release: 2git%{shortcommit0}%{?dist}
License: GPL
Group: Applications/Multimedia
URL: http://qsampler.sourceforge.net/qsampler-index.html
#Source0: http://download.linuxsampler.org/packages/qsampler-%{version}.tar.bz2
#Source0: http://downloads.sourceforge.net/qsampler/qsampler-%{version}.tar.gz
Source0: https://github.com/rncbc/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz
Source1: qsampler.desktop
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: linuxsampler
Requires: hicolor-icon-theme
Packager: Leon L. Robinson

BuildRequires: qt5-qtbase-devel qt5-linguist qt5-qtx11extras-devel
BuildRequires: libgig-devel liblscp-devel desktop-file-utils
%if 0%{?fedora} == 1
BuildRequires: XFree86-devel
%endif
BuildRequires: libtool automake autoconf

%description
QSampler is a LinuxSampler GUI front-end application written in C++
around the Qt5 toolkit using Qt Designer. At the moment it just wraps
as a client reference interface for the LinuxSampler Control Protocol
(LSCP).

%prep
%setup -q -n %{name}-%{commit0}
if [ -f Makefile.svn ]; then make -f Makefile.svn; fi

%build
%configure

%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install

# desktop file categories
BASE="Application AudioVideo Audio"
XTRA="X-Jack X-MIDI X-Synthesis Midi"

%{__mkdir} -p %{buildroot}%{_datadir}/applications
desktop-file-install --vendor %{desktop_vendor} \
  --dir %{buildroot}%{_datadir}/applications    \
  `for c in ${BASE} ${XTRA} ; do echo "--add-category $c " ; done` \
  %{SOURCE1}

%clean
%{__rm} -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null
update-desktop-database &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null
fi
update-desktop-database &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/qsampler
%{_datadir}/icons/hicolor/32x32/apps/qsampler.png
%{_datadir}/icons/hicolor/scalable/apps/qsampler.svg
%{_datadir}/icons/hicolor/32x32/mimetypes/application-x-qsampler-session.png
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-qsampler-session.svg
%{_datadir}/mime/packages/qsampler.xml
%{_datadir}/applications/%{desktop_vendor}-qsampler.desktop
%exclude %{_datadir}/applications/qsampler.desktop
#%{_datadir}/locale/qsampler_ru.qm
#%{_datadir}/locale/qsampler_cs.qm
   /usr/share/appdata/qsampler.appdata.xml
%{_mandir}/man1/qsampler.1.*
%{_datadir}/qsampler/


%changelog
* Thu Jul 21 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.4.0-1
- newer version

* Thu Jul 21 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.3.1-1
- newer version

* Fri Dec 12 2014 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.3-1
- update to latest version for fc21 build
- fix location of include file, add new files to list

* Wed May 30 2012 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.2-2.svn527
- update to latest svn for fc17 build

* Thu Sep 16 2010 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.2-2.svn507
- update to latest svn for Fedora 13, fixes segfault on startup

* Wed May 19 2010 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 
- add patch to link with -lX11 for fc13/gcc4.4.4

* Sat Nov  7 2009 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.2-1
- updated to 0.2.2

* Tue Jul  8 2008 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.1-1
- updated to 0.2.1
- add qmake to the path (otherwise the default is named qmake-qt4
  and is not found)

* Wed Nov 14 2007 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.5-1
- updated to version 0.1.5
- updated desktop categories, ignore original desktop file

* Tue Jul  3 2007 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.4-1
- updated to 0.1.4

* Wed Dec  6 2006 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.3-2
- build for fc6, spec file tweaks

* Sun Jul 30 2006 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.3-2
- built with libgig support

* Mon Jun 20 2006 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.3-1
- updated to 0.1.3
- added Planet CCRMA categories to desktop file, moved icon to proper
  freedesktop location

* Wed Jun 29 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.2-1
- updated to 0.1.2

* Thu May 26 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.1.0-1
- updated to 0.1.0, made explicit dependency on linuxsampler
- added menu entry

* Thu Jan 20 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.0.4-1 
- initial build.
