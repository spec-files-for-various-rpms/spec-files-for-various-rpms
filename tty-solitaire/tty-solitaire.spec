Name:           tty-solitaire
Version:        1.1.1
Release:        2%{?dist}
Summary:        Curses Klondike

License:        MIT
URL:            https://github.com/mpereira/tty-solitaire
Source0:        https://github.com/mpereira/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  ncurses-devel gcc-c++
#Requires:       

%description
ncurses-based klondike solitaire game.

%prep
%autosetup


%build
#configure
%make_build PREFIX="%{_prefix}"


%install
rm -rf $RPM_BUILD_ROOT
%make_install PREFIX="%{_prefix}"


%files
#license LICENSE
%doc README.md
%{_bindir}/ttysolitaire



%changelog
* Wed Oct 31 2018 L.L.Robinson <baggypants@fedoraproject.org> - 1.1.0-2
- new mock/koji requirements

* Thu Oct 25 2018 L.L.Robinson <baggypants@fedoraproject.org>
- Version 1.1.0
