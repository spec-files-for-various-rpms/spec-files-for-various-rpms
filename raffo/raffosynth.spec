%global commit0 eecc9ad7e1f86d04886852dd5ceb632c5201abc3
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global upstreamname moog
%global debug_package %{nil}

Name:           raffosynth
Version:        0.1
Release:        1.20180703git%{shortcommit0}%{?dist}
Summary:        An lv2 software minimoog

License:        MIT
URL:            https://github.com/nicoroulet/moog/
Source0:        https://github.com/nicoroulet/%{upstreamname}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires:  gcc-c++ lv2-devel gtk2-devel lv2-c++-tools-devel
Requires:       lv2

%description
A software approximation of a minimoog

%prep
%autosetup -n %{upstreamname}-%{commit0}


%build
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install -e INSTALL_DIR=%{buildroot}%{_libdir}/lv2


%files
%license LICENSE.md
%doc README.md RaffoSynth.pdf
%{_libdir}/lv2/raffo.lv2
