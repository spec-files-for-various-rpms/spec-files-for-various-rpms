Name:           samplecat
Version:        0.2.4
Release:        1%{?dist}
Summary:        Audio sample catalogue and audition tool

License:        GPL3+
URL:            http://ayyi.github.io/samplecat/
Source0:        http://www.orford.org/assets/%{name}-%{version}.tar.gz

BuildRequires:  jack-audio-connection-kit-devel libsndfile-devel gtkglext-devel libxml2-devel
BuildRequires:  libass-devel ffmpeg-devel
#Requires:       

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license COPYING
%doc NEWS README



%changelog
* Tue Feb 21 2017 L.L.Robinson <baggypants@fedoraproject.org>
- 
