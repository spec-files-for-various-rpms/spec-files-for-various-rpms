Name:		CorsixTH
Version:	0.50
Release:	4%{?dist}
Summary:	CorsixTH aims to reimplement the game engine of Theme Hospital

Group:		Games
License:	Open Source
URL:		https://github.com/CorsixTH/CorsixTH
Source0:	https://github.com/CorsixTH/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:	lua-devel, SDL2-devel, SDL2_mixer-devel, freetype-devel, cmake, auto-destdir
Requires:	lua-filesystem

%description
CorsixTH aims to reimplement the game engine of Theme Hospital, and be able to
load the original game data files. This means that you will need a purchased
copy of Theme Hospital, or a copy of the demo, in order to use CorsixTH. After
most of the original engine has been reimplemented in open source code, the
project will serve as a base from which extensions and improvements to the
original game can be made.


%prep
%setup -q


%build
#configure
%cmake  -DCMAKE_INSTALL_PREFIX=%{_datarootdir} -DWITH_MOVIES=0
%make_build

%install
%make_install
mkdir -p %{buildroot}%{_bindir}
mv %{buildroot}%{_datarootdir}/CorsixTH/CorsixTH %{buildroot}%{_bindir}

%files
%doc
%{_bindir}/CorsixTH
%{_datarootdir}/CorsixTH/*

%changelog
* Fri Feb 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.50-4
- switch it back to lua

* Fri Feb 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.50-3
- Added missing dependency

* Fri Feb 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.50-2
- Changed to use luajit

* Fri Feb 05 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.50-1
- Latest Tagged version

* Thu Jan 22 2015 Leon L. Robinson <l.l.robinson@therobinsonfamily.net>
- Initial build

* Thu Jan 22 2015 Leon L. Robinson <l.l.robinson@therobinsonfamily.net>
- messing with install dir
