Name:           libdesktop-agnostic
Version:        0.3.92
Release:        1%{?dist}
Summary:        A Library soley for use with Avant Window Navigator

License:        GPL2
URL:            https://launchpad.net/libdesktop-agnostic
Source0:        https://launchpad.net/libdesktop-agnostic/0.4/0.3.92/+download/%{name}-%{version}.tar.gz

BuildRequires:	vala-compat python2-devel intltool glib2-devel gtk2-devel
BuildRequires:	gobject-introspection-devel
#Requires:       

%description


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup


%build
./waf configure --config-backends=[cfg] --vfs-backends=[vfs] --desktop-entry-backends=[de]
./waf

%install
rm -rf $RPM_BUILD_ROOT
./waf install
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%license add-license-file-here
%doc add-main-docs-here
%{_libdir}/*.so.*

%files devel
%doc add-devel-docs-here
%{_includedir}/*
%{_libdir}/*.so


%changelog
* Wed Nov 30 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
