%global commit0 d6ff7b9c6ea2efed1682268699528e334b178d68
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global upstreamname GxSaturator.lv2
Name:           lv2-GxSaturator-plugin
Version:        0.01
Release:        1git%{shortcommit0}%{?dist}
Summary:        A saturation plugin

License:        GPLv3
URL:            https://github.com/brummer10/GxSaturator.lv2
Source0:	https://github.com/brummer10/%{upstreamname}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires:  jack-audio-connection-kit-devel lv2-devel gtk2-devel
Requires:       lv2
%description
A saturation plugin using the lv2 plugin library

%prep
%autosetup -n %{upstreamname}-%{commit0}


%build
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install INSTALL_DIR=%{_libdir}/lv2


%files
%license  LICENSE
%doc README.md
%{_libdir}/lv2/gx_saturate.lv2



%changelog
* Sun Jan 22 2017 L.L.Robinson <baggypants@fedoraproject.org>
- 
