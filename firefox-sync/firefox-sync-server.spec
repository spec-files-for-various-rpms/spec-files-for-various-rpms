Name:           syncserver
Version:        1.5.2
Release:        1%{?dist}
Summary:        Firefox Sync Server

License:        MPL2.0
URL:            https://github.com/mozilla-services/syncserver
Source0:        https://github.com/mozilla-services/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  python-devel python2-virtualenv
Requires:       python

%description


%prep
%autosetup


%build
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Wed Sep 28 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
