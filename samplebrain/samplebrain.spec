Name:           samplebrain
Version:        0.18_release
Release:        1%{?dist}
Summary:        A custom sample mashing app designed by Aphex Twin.

License:        GPLv2
URL:            https://gitlab.com/then-try-this/samplebrain
Source0:        %{url}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++ extra-cmake-modules qconf qt5-qtbase-devel
BuildRequires:  libsndfile-devel portaudio-devel liblo-devel
BuildRequires:  fftw-devel qt-creator
Requires:       null

%description


%prep
%autosetup


%build
%{qmake_qt5}

%install
rm -rf $RPM_BUILD_ROOT
install samplebrain $RPM_BUILD_ROOT/usr/bin/samplebrain

%files
%license LICENSE
%doc README.md



%changelog
* Mon Sep 26 2022 Baggypants <junk@therobinsonfamily.net>
- 
