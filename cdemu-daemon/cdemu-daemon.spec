Summary: CDEmu daemon
Name: cdemu-daemon
Version: 3.1.0
Release: 1%{?dist}
License: GPLv2
Group: System Environment/Daemons
URL: http://cdemu.sourceforge.net/about/daemon/
Source: http://downloads.sourceforge.net/cdemu/%{name}-%{version}.tar.bz2

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: glib2-devel
BuildRequires: libao-devel
BuildRequires: libmirage-devel
BuildRequires: cmake

Requires: libmirage
Requires: vhba

%description
CDEmu daemon implements the userspace part of virtual CD/DVD-ROM device
used by CDEmu, a CD/DVD-ROM device emulator for linux. It receives packet
commands from VHBA kernel module, processes them and passes resulting data
back to the kernel. It also provides D-Bus interface for controlling virtual
devices.

%prep
%setup -q

%build
%cmake .
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)
%doc AUTHORS COPYING README ChangeLog
%doc %_mandir/man8/*
%{_bindir}/*
%{_libexecdir}/*
%{_datadir}/dbus-1/services/*

%changelog
* Wed Jun 22 2016 L.L.Robinson <baggypants@fedoraproject.org> - 3.0.4-1
- latest version

* Wed Nov 04 2015 L.L.Robinson <baggypants@fedoraproject.org> - 3.0.2-2
- add missing cmake dep

* Sun Sep 28 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.2-1
- Updated to 3.0.2

* Fri Jul 25 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.1-1
- Updated to 3.0.1

* Sun Jun 29 2014 Rok Mandeljc <rok.mandeljc@gmail.com> - 3.0.0-1
- Updated to 3.0.0

* Thu Sep 19 2013 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.1.1-1
- Updated to 2.1.1

* Fri Jun  7 2013 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.1.0-1
- Updated to 2.1.0

* Mon Dec 24 2012 Rok Mandeljc <rok.mandeljc@gmail.com> - 2.0.0-1
- RPM release for new version
