Name:           navit
Version:        0.5.6
Release:        2%{?dist}
Summary:        Turn-by-turn navigation software 

License:        GPLv2
URL:            http://www.navit-project.org/  
Source0:        https://github.com/%{name}-gps/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz          

BuildRequires:  cmake gcc-c++ speech-dispatcher 
#protobuf-c-devel 
BuildRequires: 	gettext-devel libtool glib2-devel cegui-devel freeglut-devel quesoglc-devel 
BuildRequires:  SDL-devel libXmu-devel gpsd-devel gtk2-devel speech-dispatcher-devel cvs 
BuildRequires:  python-devel geoclue2-devel desktop-file-utils dbus-glib-devel
#Requires:       

%description
Turn-by-turn navigation software


%prep
%autosetup


%build
%cmake -DSAMPLE_MAP=n
%cmake_build


%install
%cmake_install
desktop-file-validate %{buildroot}/%{_datadir}/applications/navit.desktop
%find_lang %{name}

%check
%ctest

%files
%license COPYING
%doc README.md
%{_bindir}/navit
#%{_bindir}/maptool
%{_libdir}/navit
%{_datadir}/icons/hicolor/*/apps/navit.png
%{_datadir}/navit
%{_datadir}/applications/navit.desktop
%{_datadir}/locale/*/LC_MESSAGES/navit.mo
%{_datadir}/dbus-1/services/org.navit_project.navit.service
%{_mandir}/man1/maptool.1*
%{_mandir}/man1/navit.1*


%changelog
* Wed Oct 20 2021 Baggypants <junk@therobinsonfamily.net> - 0.5.6-2
- removed maptool

* Wed Oct 20 2021 Baggypants <junk@therobinsonfamily.net>
- 
