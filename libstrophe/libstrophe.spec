Name:           libstrophe
Version:        0.9.2
Release:        1%{?dist}
Summary:        A simple, lightweight C library for writing XMPP clients

License:        GPLv3 or MIT
URL:            http://strophe.im/libstrophe
Source0:        https://github.com/strophe/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  libxml2-devel autoconf automake libtool openssl-devel expat-devel gcc

%description
A minimal XMPP library written in C. It has almost no external dependencies, 
only an XML parsing library (expat or libxml are both supported). It is 
designed for both POSIX and Windows systems.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup


%build
./bootstrap.sh
%configure --disable-static
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%license GPL-LICENSE.txt MIT-LICENSE.txt
%doc README.markdown
%{_libdir}/libstrophe.so.*

%files devel
%doc 
%{_includedir}/strophe.h
%{_libdir}/pkgconfig/libstrophe.pc
%{_libdir}/libstrophe.so


%changelog
* Mon May 06 2019 L.L.Robinson <baggypants@fedoraproject.org> - 0.9.2-1
- new release

* Thu Nov 10 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.9.1-4
- adjusted some packaging issues

* Sat Oct 15 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.9.1-3
- silly typo in files section

* Sat Oct 15 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.9.1-2
- fixed files section not to be greedy

* Mon Aug 22 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
