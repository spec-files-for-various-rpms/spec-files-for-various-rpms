Name:           ring
Version:        20161116.1.e59aaa5
Release:        1%{?dist}
Summary:        Free software for universal communication which respects freedoms and privacy of its users.

License:        GPL
URL:            http://ring.cx
Source0:        http://dl.ring.cx/ring-release/tarballs/%{name}_%{version}.tar.gz

#BuildRequires:  
#Requires:       

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Wed Nov 23 2016 L.L.Robinson <baggypants@fedoraproject.org>
- 
