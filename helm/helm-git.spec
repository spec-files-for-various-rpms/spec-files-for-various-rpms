Name:		helm
Version:	0.4.1
Release:        3.20160101gitd06c276c%{?dist}
Summary:	Polyphonic software synth with lots of modulation and easy to use UI

Group:		Applications/Multimedia
License:	GPLv3
URL:		http://tytel.org/helm/
Source0:        helm-d06c276c.tar
Source1:	%{name}.desktop

BuildRequires:	mesa-libGL-devel, alsa-lib-devel, jack-audio-connection-kit-devel, freetype-devel, libXrandr-devel, libXinerama-devel, libXcursor-devel, desktop-file-utils
Requires:	%{name}-common = %{version}

%description
Helm is a software synth designed to be easy to use

%package lv2
Summary:	Helm lv2 plugin
Group:		Applications/Multimedia
Requires: 	%{name}-common = %{version}
%description lv2
Helm is a polyphonic software synth with lots of modulation and and easy to use UT

%package common
Summary: 	Helm common files
BuildArch: 	noarch
Group: 		Applications/Multimedia
%description common
Common files for the helm software synth.


%prep
%autosetup -n helm


%build
%configure
%make_build


%install
%make_install LIBDIR=%{_libdir}


desktop-file-install                                    \
--dir=%{buildroot}%{_datadir}/applications         \
%{SOURCE1}
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/128x128/apps
cp %{buildroot}"%{_datadir}/helm/icons/helm_icon_128_1x.png" %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/helm.png


rm %{buildroot}%{_datadir}/helm/patches/Synth/.DS_Store


%files
%doc
%{_bindir}/%{name}
%{_datadir}/applications/helm.desktop
%{_datadir}/icons/hicolor/128x128/apps/helm.png


%files lv2
%{_libdir}/lv2/helm.lv2/

%files common
%doc README.md
%license COPYING
%{_datadir}/helm/



%changelog
* Fri Jan 01 2016 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-3.20160101gitd06c276c
- Update to git: d06c276c

* Wed Nov 18 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-2.20151118gitad798d4a
- Update to git: ad798d4a

* Sat Aug 22 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-7.20150822git614968de
- Update to git: 614968de

* Sat Aug 22 2015 Leon L. Robinson <baggypants@fedoraproject.org> - 0.4.1-6
- added exclusion for armv7hl

* Thu Aug 06 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-5
- corrected url in info

* Thu Aug 06 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-4
- fixed -common to noarch
- fixed requires to specify version of -common
- replaced all occurences of RPM_BUILD_ROOT
- updated make and setup with modern equivelents
- simplified the files secion
- correctly specified the licence file
- fixed incorrect licence in info
- corrected icon location from pixmaps to icons/hicolor
- simplified Requires:


* Wed Aug 05 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-3
- Fix icon

* Tue Aug 04 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-2
- added docs
- added desktop file

* Sun Aug 02 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-1
- Update to 0.4.1


