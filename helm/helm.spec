#global commit0 19f86e6b4db83c1c6b143fc27883592ac4e43489
#global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
Name:		helm
Version:	0.9.0
#Release:	1%{?dist}
Release:	1%{?dist}
Summary:	Polyphonic software synthesizer

Group:		Applications/Multimedia
License:	GPLv3+
URL:		http://tytel.org/helm/
Source0:	https://github.com/mtytel/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
#Source0:	https://github.com/mtytel/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz
Source1:	%{name}.desktop
Source2:	%{name}.appdata.xml

BuildRequires:	mesa-libGL-devel, alsa-lib-devel, jack-audio-connection-kit-devel, freetype-devel, libXrandr-devel, libXinerama-devel, libXcursor-devel, desktop-file-utils, google-roboto-fonts, google-droid-sans-mono-fonts
BuildRequires:	libappstream-glib libcurl-devel
Requires:	%{name}-common = %{version}-%{release}, google-roboto-fonts, google-droid-sans-mono-fonts
Requires:	hicolor-icon-theme
Provides: bundled(JUCE) = 3.3.0
ExcludeArch:	armv7hl
#Does not compile on arm at the moment, the developer isn't sure why
#but it is probably to do with the JUCE library

%description
Helm is a polyphonic software synthesizer with lots of modulation and an easy 
to use UI

%package -n lv2-helm
Summary:	Helm lv2 plugin
Group:		Applications/Multimedia
Requires:	%{name}-common = %{version}-%{release}
Requires:	lv2%{?_isa}
%description -n lv2-helm
Helm is a polyphonic software synthesizer plugin.

%package common
Summary:	Helm common files
BuildArch:	noarch
Group:		Applications/Multimedia
%description common
Common files for the helm software synthesizer.

%package vst
Summary:	Helm vst plugin
Group:		Applications/Multimedia
Requires:	%{name}-common = %{version}-%{release}

%description vst
Helm vst is a polyphonic software synthesizer plugin.

%prep
%autosetup
#autosetup -n %{name}-%{commit0}

%build
%configure
%make_build


%install
%make_install LIBDIR=%{_libdir}


desktop-file-install \
--dir=%{buildroot}%{_datadir}/applications \
%{SOURCE1}
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/128x128/apps
cp %{buildroot}"%{_datadir}/helm/icons/helm_icon_128_1x.png" %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/helm.png

mkdir -p %{buildroot}%{_datadir}/appdata/
cp %{SOURCE2} %{buildroot}%{_datadir}/appdata/

appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/appdata/*.appdata.xml

mv  $RPM_BUILD_ROOT%{_datadir}/doc/helm __doc
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/helm



%files
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/appdata/%{name}.appdata.xml
<<<<<<< HEAD
%{_mandir}/man1/helm.1.*

=======
%{_mandir}/man1/helm.1*
%{_docdir}/helm/changelog.gz
>>>>>>> 6e9b97d400685c118220448433ab002ba2cf4be1

%files -n lv2-helm
%{_libdir}/lv2/helm.lv2/

%files common
<<<<<<< HEAD
%doc README.md __doc/*
=======
%doc README.md 
>>>>>>> 6e9b97d400685c118220448433ab002ba2cf4be1
%license COPYING
%{_datadir}/helm/

%files vst
%{_libdir}/lxvst/helm.so

%changelog
* Wed Sep 06 2017 L.L.Robinson <baggypants@fedoraproject.org> - 0.9.0-1
- latest version

* Thu Feb 11 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.6.1-1
- Update to latest tagged version

* Thu Jan 28 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.6.0-3
- fixed some more packaging things

* Sat Jan 02 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.6.0-2
- dumb font version removed

* Sat Jan 02 2016 L.L.Robinson <baggypants@fedoraproject.org> - 0.6-1
- New version 0.6

* Fri Oct 23 2015 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-3
- fixed more packaging issues

* Thu Sep 17 2015 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-2
- patch to increase build verbosity

* Mon Sep 14 2015 L.L.Robinson <baggypants@fedoraproject.org> - 0.5.0-1
- update to version 0.5.0

* Sun Aug 30 2015 L.L.Robinson <baggypants@fedoraproject.org> - 0.4.1-7
- Made changes from review

* Sat Aug 22 2015 Leon L. Robinson <baggypants@fedoraproject.org> - 0.4.1-6
- added exclusion for armv7hl

* Thu Aug 06 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-5
- corrected url in info

* Thu Aug 06 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-4
- fixed -common to noarch
- fixed requires to specify version of -common
- replaced all occurences of RPM_BUILD_ROOT
- updated make and setup with modern equivelents
- simplified the files secion
- correctly specified the licence file
- fixed incorrect licence in info
- corrected icon location from pixmaps to icons/hicolor
- simplified Requires:


* Wed Aug 05 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-3
- Fix icon

* Tue Aug 04 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-2
- added docs
- added desktop file

* Sun Aug 02 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - 0.4.1-1
- Update to 0.4.1


