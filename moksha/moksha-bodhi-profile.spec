Name:		bodhi3packages
Version:	0.1
Release:	1%{?dist}
Summary:	Profile files for Moksha

Group:		Testing
License:	GPLv2
URL:		https://github.com/Baggypants/bodhi3packages
Source0:	https://github.com/Baggypants/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildArch:	noarch
BuildRequires:	/usr/bin/cp
Requires:	bash

%description
Configuration files for Moksha

%prep
%setup -q


%build



%install
mkdir -p %{buildroot}%{_datadir}/
cp -r bodhi-profile-moksha/usr/share/enlightenment %{buildroot}%{_datadir}/

%files
%doc
%{_datadir}/enlightenment/data/config/bodhi/


%changelog

