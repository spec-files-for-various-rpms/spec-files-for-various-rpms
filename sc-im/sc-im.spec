Name:           sc-im
Version:        0.7.0
Release:        5%{?dist}
Summary:        SC-IM is a spreadsheet program that is based on SC

License:        SC-IM Licence, Open Source
URL:            https://github.com/andmarti1424/sc-im
Source0:        https://github.com/andmarti1424/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires: byacc  ncurses-devel gcc-c++
#Requires:       

%description
SCIM uses ncurses for visual interface and has vim-like keybindings and some functional similarities with vim text editor.


%prep
%autosetup


%build
#configure
#make_build
cd src
make -j1 prefix=%{_prefix}


%install
rm -rf $RPM_BUILD_ROOT
cd src
%make_install prefix=%{_prefix}
#make install DESTDIR=%{buildroot}


%files
%license LICENSE
%doc Readme.md
%{_bindir}/scim
%{_mandir}/man1/scim.1.gz
%{_datadir}/scim



%changelog
* Thu Oct 18 2018 L.L.Robinson <baggypants@fedoraproject.org> - 0.7.0-5
- build works now yay

* Fri Oct 12 2018 L.L.Robinson <baggypants@fedoraproject.org>
- 
