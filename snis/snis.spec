%global commit0 cacf3d1d7721916029b483f10df5315300b23103
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
Name:		space-nerds-in-space
Version:	0.0.1
Release:        7git%{shortcommit0}%{?dist}
Summary:	A GAME

Group:		Entertaiment/Games
License:	GPLv2
URL:		http://smcameron.github.io/space-nerds-in-space/
Source0:        http://github.com/smcameron/%{name}/archive/%{commit0}.tar.gz#/%{name}-%{shortcommit0}.tar.gz

BuildRequires:	portaudio-devel, libvorbis-devel, gtk2-devel, git, openscad, gtkglext-devel, lua-devel, glew-devel, SDL-devel
BuildRequires:	openssl-devel
Requires:	bash

%description
Space Nerds in Space

%prep
%setup -q -n %{name}-%{commit0}


%build
#configure
make %{?_smp_mflags} PREFIX=/usr


%install
%make_install PREFIX=/usr


%files
%doc
%{_bindir}/snis_client
%{_bindir}/snis_limited_client
%{_bindir}/snis_server
%{_bindir}/ssgl_server
%{_bindir}/snis_multiverse
%{_bindir}/snis_text_to_speech.sh
%{_datadir}/applications/snis.desktop
%{_mandir}/man6/snis_client.6*
%{_mandir}/man6/snis_server.6*
%{_mandir}/man6/earthlike.1*
%{_mandir}/man6/gaseous-giganticus.1*
%{_mandir}/man6/snis_text_to_speech.sh.6*
%{_datadir}/snis/


%changelog
* Mon Jan 23 2017 L.L.Robinson <baggypants@fedoraproject.org> - 0.0.1-7git230c3e8
- new git version

* Fri Jan 01 2016 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-6.20160101git4c3988ad
- Update to git: 4c3988ad

* Sat Dec 05 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-5.20151205git22b3701f
- Update to git: 22b3701f

* Wed Nov 18 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-4.20151118git7fa133c9
- Update to git: 7fa133c9

* Sat Aug 22 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-3.20150822gitbdda75c4
- Update to git: bdda75c4

* Tue Aug 11 2015 L.L.Robinson <l.l.robinson@therobinsonfamily.net> - git-2.20150811git359f1b49
- Update to git: 359f1b49


