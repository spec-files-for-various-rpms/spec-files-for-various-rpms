%global debug_package %{nil}

Name:           dragonfly-reverb
Version:        1.1.4
Release:        1%{?dist}
Summary:        Hall reverb

License:        GPL3
URL:            https://github.com/michaelwillis/%{name}.git
Source0:        https://github.com/michaelwillis/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  gcc gcc-c++ jack-audio-connection-kit-devel libX11-devel mesa-libGL-devel
BuildRequires:  mesa-libEGL-devel
Requires:       lv2%{?_isa}

%description
A free hall-style reverb based on freeverb3 algorithms

%prep
%autosetup


%build
%make_build


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_libdir}/lv2/DragonflyReverb.lv2
install bin/DragonflyReverb.lv2/* $RPM_BUILD_ROOT%{_libdir}/lv2/DragonflyReverb.lv2/

%files
%license LICENSE
%doc README.md
%{_libdir}/lv2/DragonflyReverb.lv2



%changelog
* Mon May 06 2019 L.L.Robinson <baggypants@fedoraproject.org> - 1.1.4-1
- New version

* Wed Dec 12 2018 L.L.Robinson <baggypants@fedoraproject.org>
- 
