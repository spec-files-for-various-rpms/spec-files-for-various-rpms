Name:           gopaste
Version:        1.0.1
Release:        1%{?dist}
Summary:        paste for p.lee.io

License:        paste cli for p.lee.io
URL:            https://github.com/Lee303/gopaste
Source0:        https://github.com/Lee303/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz


BuildRequires:  gcc
BuildRequires:  golang

BuildRequires:  golang(gopkg.in/Luzifer/go-openssl.v1)
#Requires:       

%description
paste for p.lee.io

%prep
%autosetup


%build
mkdir -p ./_build/src/github.com/example
ln -s $(pwd) ./_build/src/github.com/example/app
export GOPATH=$(pwd)/_build:%{gopath}
go build -o example-app .


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Tue Nov 20 2018 L.L.Robinson <baggypants@fedoraproject.org>
- 
